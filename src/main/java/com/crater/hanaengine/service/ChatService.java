package com.crater.hanaengine.service;

import com.crater.hanaengine.bean.dto.ChatDto;
import com.crater.hanaengine.bean.dto.ChatResultDo;

public interface ChatService {
    ChatResultDo takeChat(ChatDto chatDto);
}
