package com.crater.hanaengine.service.impl;

import com.crater.hanaengine.bean.dto.InstructionDto;
import com.crater.hanaengine.enums.Level;
import com.crater.hanaengine.enums.Function;
import com.crater.hanaengine.exception.HanaInstructionException;
import com.crater.hanaengine.service.InstructionToObjectService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class InstructionToObjectServiceImpl implements InstructionToObjectService {

    @Override
    public InstructionDto parsingInstruction(String instruction) {
        InstructionDto result = new InstructionDto();
        String[] instructionArray = instruction.split("\\.");
        if (!instructionArray[0].matches("^hana$")) throw new HanaInstructionException("this is not Instruction");
        String levelString = instructionArray[1];
        String functionString = instructionArray[2];
        result.setLevel(gettingLevel(levelString)
                .orElseThrow(() -> new HanaInstructionException("Level not found")));
        result.setFunction(gettingFunction(functionString, result.getLevel())
                .orElseThrow(() -> new HanaInstructionException("function name not found")));
        result.setValue(gettingFunctionValue(instruction));
        return result;
    }

    private Optional<Level> gettingLevel(String levelContent) {
        Level result = null;
        if (StringUtils.isEmpty(levelContent)) throw new HanaInstructionException("Level can't be null");
        for (Level level : Level.values()) {
            if (level.getLevelContent().equals(levelContent)) result = level;
        }
        return Optional.ofNullable(result);
    }

    private Optional<Function> gettingFunction(String functionString, Level expectedUseLevel) {
        functionString = functionString.replaceAll("\\(.*", "");
        Function result = null;
        if (StringUtils.isEmpty(functionString)) throw new HanaInstructionException("taxonomy can't be null");
        for (Function t : Function.values()) {
            if (t.getFunctionString().equals(functionString) &&
                    checkFunctionIsLegitimate(t, expectedUseLevel)) result = t;
        }
        return Optional.ofNullable(result);
    }

    private boolean checkFunctionIsLegitimate(Function function, Level expectedUseLevel) {
        Level[] levelAllowedByTaxonomy = function.getCorrespondingLevel();
        boolean result = false;
        for (Level level : levelAllowedByTaxonomy) {
            if (level.equals(expectedUseLevel)) {
                result = true;
                break;
            }
        }
        return result;
    }

    private String gettingFunctionValue(String instructionString) {
        return instructionString.replaceAll(".*\\(\"", "")
                .replaceAll("\"\\)$", "");
    }

}
