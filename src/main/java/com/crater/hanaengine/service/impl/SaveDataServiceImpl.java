package com.crater.hanaengine.service.impl;

import com.crater.hanaengine.bean.configurationProperties.ScriptProperties;
import com.crater.hanaengine.bean.db.SaveTablePo;
import com.crater.hanaengine.bean.db.ScriptTablePo;
import com.crater.hanaengine.bean.dto.GetSaveDataDto;
import com.crater.hanaengine.bean.dto.UpdateSaveDataDto;
import com.crater.hanaengine.dao.SaveDao;
import com.crater.hanaengine.dao.ScriptFileDao;
import com.crater.hanaengine.exception.HanaInstructionException;
import com.crater.hanaengine.service.SaveDataService;
import com.crater.hanaengine.service.ScriptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.*;

@Service
public class SaveDataServiceImpl implements SaveDataService {
    private SaveDao saveDao;
    private ScriptFileDao scriptFileDao;
    private ScriptService scriptService;
    private ScriptProperties scriptProperties;

    @Override
    public List<GetSaveDataDto> queryAllSaveData() {
        try {
            var saveTablePos = saveDao.queryAllSaveData();
            return saveTablePos.orElseThrow(() -> new RuntimeException("no save data found")).stream().map(s -> {
                var currentPath = "/" + s.scriptFileName() + "/" + s.scriptFileLineNumber();
                var nextPath = generateNexPath(s.scriptFileName(), s.scriptFileLineNumber());
                return converter2SaveDataDto(s, currentPath, nextPath);
            }).toList();
        } catch (Exception e) {
            throw new RuntimeException("query all save data fail", e);
        }
    }

    private String generateNexPath(String scriptFileName, Long scriptFileLineNumber) {
        try {
            var scriptFolderName = scriptProperties.getScriptFolderName();
            var path = Paths.get("");
            var fullScriptPath = path.toAbsolutePath().normalize() + "/" + scriptFolderName + "/" + scriptFileName;
            var scriptDto = scriptService.readScriptContentAndInstruction(fullScriptPath, scriptFileLineNumber);
            if (!scriptDto.isBottomOfFile())
                return "/" + scriptFileName + "/" + (scriptDto.getScriptFileEndLineNumber() + 1);
            else return null;
        } catch (Exception e) {
            throw new HanaInstructionException("generate next path fail", e);
        }
    }

    private GetSaveDataDto converter2SaveDataDto(SaveTablePo saveTablePo, String currentPath, String nextPath) {
        return new GetSaveDataDto(saveTablePo.id(), saveTablePo.remarks(), currentPath, nextPath,
                saveTablePo.backgroundFileName(), saveTablePo.role1FileName(), saveTablePo.role2FileName(),
                saveTablePo.role3FileName(), saveTablePo.bgmFileName(), saveTablePo.currentText(),
                saveTablePo.createDate(), saveTablePo.updateDate());
    }

    @Override
    public GetSaveDataDto querySaveData(String saveId) {
        return null;
    }

    @Transactional
    @Override
    public void newSaveData(UpdateSaveDataDto updateSaveDataDto) {
        try {
            insertSaveTable(updateSaveDataDto);
            insertScriptTable(updateSaveDataDto);
            throw new RuntimeException("for test");
        } catch (Exception e) {
            throw new RuntimeException("save new data fail", e);
        }
    }

    private void insertSaveTable(UpdateSaveDataDto updateSaveDataDto) {
        saveDao.insertData(SaveTablePo.converter2SaveTablePo(updateSaveDataDto));
    }

    private void insertScriptTable(UpdateSaveDataDto updateSaveDataDto) {
        var currentPath = updateSaveDataDto.currentPath();
        var scriptName = currentPath.split("/")[0];
        var lineNumber = Long.valueOf(currentPath.split("/")[1]);
        var newScriptData = new ScriptTablePo().setScriptFileName(scriptName)
                .setScriptFileLineNumber(lineNumber).setSaveId(updateSaveDataDto.saveId())
                .setCreateDate(LocalDateTime.now());
        scriptFileDao.insertData(newScriptData);
    }

    @Transactional
    @Override
    public void updateSaveData(UpdateSaveDataDto updateSaveDataDto) {
        try {
            updateSaveTable(updateSaveDataDto);
        } catch (Exception e) {
            throw new RuntimeException("update save data fail", e);
        }
    }

    private void updateSaveTable(UpdateSaveDataDto updateSaveDataDto) {
        saveDao.updateData(SaveTablePo.converter2SaveTablePo(updateSaveDataDto));
    }

    @Autowired
    public SaveDataServiceImpl setSaveDao(SaveDao saveDao) {
        this.saveDao = saveDao;
        return this;
    }

    @Autowired
    public SaveDataServiceImpl setScriptFileDao(ScriptFileDao scriptFileDao) {
        this.scriptFileDao = scriptFileDao;
        return this;
    }

    @Autowired
    public SaveDataServiceImpl setScriptService(ScriptService scriptService) {
        this.scriptService = scriptService;
        return this;
    }

    @Autowired
    public SaveDataServiceImpl setScriptProperties(ScriptProperties scriptProperties) {
        this.scriptProperties = scriptProperties;
        return this;
    }
}
