package com.crater.hanaengine.service.impl;

import com.crater.hanaengine.bean.configurationProperties.ScriptProperties;
import com.crater.hanaengine.bean.dto.*;
import com.crater.hanaengine.enums.Function;
import com.crater.hanaengine.enums.Level;
import com.crater.hanaengine.exception.HanaApiException;
import com.crater.hanaengine.exception.HanaInstructionException;
import com.crater.hanaengine.exception.HanaScriptException;
import com.crater.hanaengine.service.ChatService;
import com.crater.hanaengine.service.InstructionToObjectService;
import com.crater.hanaengine.service.ScriptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ChatServiceImpl implements ChatService {
    private ScriptService scriptService;
    private InstructionToObjectService instructionToObjectService;
    private ScriptProperties scriptProperties;

    @Override
    public ChatResultDo takeChat(ChatDto chatDto) {
        try {
            var scriptFileName = chatDto.getScriptFileName();
            var lineNumber = chatDto.getLineNumber();
            var scriptFileUrl = generateScriptFilePah(scriptFileName);
            var readFileDto = scriptService.readScriptContentAndInstruction(scriptFileUrl, lineNumber);
            var chatContent = readFileDto.getScriptContent().orElse(null);
            var instruction = readFileDto.getScriptInstruction();
            var instructionDtos = new ArrayList<InstructionDto>();
            instruction.ifPresent(strings ->
                    strings.forEach(s -> instructionDtos.add(instructionToObjectService.parsingInstruction(s))));
            var nextPath = generateNextPath(readFileDto);
            var soundEffect = takeSoundEffect(instructionDtos);
            var backgroundImage = takeBackgroundImage(instructionDtos);
            var role1 = takeChatRequestForRole(1, instructionDtos);
            var role2 = takeChatRequestForRole(2, instructionDtos);
            var role3 = takeChatRequestForRole(3, instructionDtos);
            var cutscene = takeCutscene(instructionDtos);
            var bgmFileName = takeBgm(instructionDtos).orElse(null);
            return new ChatResultDo().setNextPath(nextPath).setCutscene(cutscene).setChatContent(chatContent)
                    .setBackgroundImage(backgroundImage).setBgmFileName(bgmFileName).setSoundEffect(soundEffect)
                    .setRole1(role1).setRole2(role2).setRole3(role3);
        } catch (HanaInstructionException e) {
            throw e;
        } catch (Exception e) {
            throw new HanaApiException("get chat have unknown error", e);
        }
    }

    private String generateNextPath(ScriptDto scriptDto) {
        try {
            return scriptDto.isBottomOfFile() ? null :
                    "/" + scriptDto.getScriptName() + "/" + (scriptDto.getScriptFileEndLineNumber() + 1);
        } catch (Exception e) {
            throw new HanaInstructionException("generate next path fail", e);
        }
    }

    private String generateScriptFilePah(String scriptFileName) {
        try {
            var scriptFolderName = scriptProperties.getScriptFolderName();
            var path = Paths.get("");
            return path.toAbsolutePath().normalize() + "/" + scriptFolderName + "/" + scriptFileName;
        } catch (Exception e) {
            throw new HanaScriptException("generate script file path have unknown error", e);
        }
    }

    private String takeBackgroundImage(List<InstructionDto> instructionDtos) {
        for (var i : instructionDtos) {
            if (i.getLevel().equals(Level.background) && i.getFunction().equals(Function.fileName))
                return i.getValue();
        }
        return null;
    }

    private String takeCutscene(List<InstructionDto> instructionDtos) {
        for (var i : instructionDtos) {
            if (i.getLevel().equals(Level.system) && i.getFunction().equals(Function.cutscene))
                return i.getValue();
        }
        return null;
    }

    private Optional<String> takeBgm(List<InstructionDto> instructionDtos) {
        var bgm = instructionDtos.parallelStream()
                .filter(i -> Level.backgroundMusic.equals(i.getLevel()) &&
                        Function.fileName.equals(i.getFunction())).toList();
        if (bgm.size() > 1) throw new HanaScriptException("Multiple setting BGM detected at once");
        return bgm.size() == 0 ? Optional.empty() : Optional.of(bgm.get(0).getValue());
    }

    private String takeSoundEffect(List<InstructionDto> instructionDtos) {
        return instructionDtos.parallelStream()
                .filter(i -> Level.sound.equals(i.getLevel()) && Function.fileName.equals(i.getFunction()))
                .map(InstructionDto::getValue).collect(Collectors.joining(","));
    }

    private Optional<RoleDto> takeChatRequestForRole(int roleNumber, List<InstructionDto> instructionDtos) {
        RoleDto result = null;
        for (InstructionDto instructionDto : instructionDtos) {
            if (roleNumber == 1 && !instructionDto.getLevel().equals(Level.role1)) continue;
            else if (roleNumber == 2 && !instructionDto.getLevel().equals(Level.role2)) continue;
            else if (roleNumber == 3 && !instructionDto.getLevel().equals(Level.role3)) continue;
            if (instructionDto.getLevel().getLevelContent().matches("role.*") && result == null)
                result = new RoleDto();
            if (result != null && instructionDto.getFunction().equals(Function.fileName))
                result.setFileName(instructionDto.getValue());
            else if (result != null && instructionDto.getFunction().equals(Function.cutscene))
                result.setCutscene(instructionDto.getValue());
            else if (result != null && instructionDto.getFunction().equals(Function.jumpAttachedComponentsItem))
                result.setJumpAttachedComponentsItem(instructionDto.getValue());
            else if (result != null && instructionDto.getFunction().equals(Function.action))
                result.setAction(instructionDto.getValue());
        }
        return Optional.ofNullable(result);
    }

    @Autowired
    public ChatServiceImpl setScriptService(ScriptService scriptService) {
        this.scriptService = scriptService;
        return this;
    }

    @Autowired
    public ChatServiceImpl setInstructionToObjectService(InstructionToObjectService instructionToObjectService) {
        this.instructionToObjectService = instructionToObjectService;
        return this;
    }

    @Autowired
    public ChatServiceImpl setScriptProperties(ScriptProperties scriptProperties) {
        this.scriptProperties = scriptProperties;
        return this;
    }
}
