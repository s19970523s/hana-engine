package com.crater.hanaengine.service.impl;

import com.crater.hanaengine.bean.dto.ScriptDto;
import com.crater.hanaengine.exception.HanaScriptException;
import com.crater.hanaengine.service.ScriptService;
import com.crater.hanaengine.utils.ReadScriptFileUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ScriptServiceImpl implements ScriptService {

    @Override
    public ScriptDto readScriptContentAndInstruction(String scriptFilePath, Long targetLineNumber) {
        var result = new ScriptDto();
        var instruction = new ArrayList<String>();
        var allScriptContent = readScript(scriptFilePath);
        checkLinNumberIsBigThenFile(targetLineNumber, allScriptContent);

        for (var i = targetLineNumber.intValue(); i < allScriptContent.size(); i++) {
            var s = allScriptContent.get(i);
            if (checkIsInstruction(s)) instruction.add(s);
            else {
                var content = formatContent(s);
                result.setScriptContent(content).setScriptFileEndLineNumber(i);
                break;
            }
        }
        return result.setScriptInstruction(instruction.size() > 0 ? instruction : null)
                .setScriptName(scriptFilePath.replaceAll(".*/", ""))
                .setBottomOfFile(result.getScriptFileEndLineNumber() + 1  == allScriptContent.size());
    }

    private void checkLinNumberIsBigThenFile(long targetLineNumber, List<String> allScriptContent) {
        if (targetLineNumber + 1 > allScriptContent.size())
            throw new HanaScriptException("targetLineNumber is over to allScriptContent size");
    }

    private List<String> readScript(String scriptFilePath) {
        return ReadScriptFileUtil.readScript(scriptFilePath)
                .orElseThrow(() -> new HanaScriptException("ReadScriptFileUtil readScript fail"));
    }

    private boolean checkIsInstruction(String scriptContent) {
        Optional.ofNullable(scriptContent)
                .orElseThrow(() -> new HanaScriptException("scriptContent is null"));
        return scriptContent.matches("^hana\\..*");
    }

    private String formatContent(String content) {
        if (StringUtils.isEmpty(content)) return content;
        var result = new StringBuilder(content);
        var colonPosition = result.indexOf(":");
        if (colonPosition < 0) colonPosition = result.indexOf("：");
        if (colonPosition > 0) result.insert(colonPosition + 1, "\n  ");
        return result.toString();
    }
}
