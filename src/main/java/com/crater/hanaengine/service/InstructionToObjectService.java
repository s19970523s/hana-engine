package com.crater.hanaengine.service;

import com.crater.hanaengine.bean.dto.InstructionDto;

public interface InstructionToObjectService {
    InstructionDto parsingInstruction(String instruction);
}
