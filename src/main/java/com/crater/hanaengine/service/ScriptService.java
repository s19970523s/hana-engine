package com.crater.hanaengine.service;

import com.crater.hanaengine.bean.dto.ScriptDto;

public interface ScriptService {
    ScriptDto readScriptContentAndInstruction(String scriptFilePath, Long targetLineNumber);
}
