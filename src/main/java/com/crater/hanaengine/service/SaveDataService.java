package com.crater.hanaengine.service;

import com.crater.hanaengine.bean.dto.GetSaveDataDto;
import com.crater.hanaengine.bean.dto.UpdateSaveDataDto;

import java.util.List;

public interface SaveDataService {
    List<GetSaveDataDto> queryAllSaveData();
    GetSaveDataDto querySaveData(String saveId);
    void newSaveData(UpdateSaveDataDto updateSaveDataDto);
    void updateSaveData(UpdateSaveDataDto updateSaveDataDto);
}
