package com.crater.hanaengine.exception;

public class HanaDaoException extends RuntimeException{
    public HanaDaoException(String message) {
        super(message);
    }

    public HanaDaoException(String message, Throwable cause) {
        super(message, cause);
    }
}
