package com.crater.hanaengine.exception;

public class FrontEndException extends RuntimeException {
    public FrontEndException() {
        super();
    }

    public FrontEndException(String message) {
        super(message);
    }

    public FrontEndException(String message, Throwable cause) {
        super(message, cause);
    }
}
