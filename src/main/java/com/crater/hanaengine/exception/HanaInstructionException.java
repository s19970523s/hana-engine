package com.crater.hanaengine.exception;

public class HanaInstructionException extends RuntimeException{
    public HanaInstructionException() {
        super();
    }

    public HanaInstructionException(String message, Throwable cause) {
        super(message, cause);
    }

    public HanaInstructionException(String errorMessage) {
        super(errorMessage);
    }
}
