package com.crater.hanaengine.exception;

public class HanaApiException extends RuntimeException{
    public HanaApiException(String message) {
        super(message);
    }

    public HanaApiException(String message, Throwable cause) {
        super(message, cause);
    }
}
