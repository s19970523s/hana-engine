package com.crater.hanaengine.exception;

public class HanaScriptException extends RuntimeException {
    public HanaScriptException() {
        super();
    }

    public HanaScriptException(String message, Throwable cause) {
        super(message, cause);
    }

    public HanaScriptException(String errorMessage) {
        super(errorMessage);
    }
}
