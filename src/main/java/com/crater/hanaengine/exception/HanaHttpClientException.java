package com.crater.hanaengine.exception;

public class HanaHttpClientException extends RuntimeException{

    public HanaHttpClientException() {
        super();
    }

    public HanaHttpClientException(String errorMessage) {
        super(errorMessage);
    }

    public HanaHttpClientException(String message, Throwable cause) {
        super(message, cause);
    }
}
