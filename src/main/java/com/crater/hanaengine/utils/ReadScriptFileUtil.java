package com.crater.hanaengine.utils;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

public class ReadScriptFileUtil {
    private static final Logger log = LoggerFactory.getLogger(ReadScriptFileUtil.class);
    public static Optional<List<String>> readScript(String filePath) {
        Optional<List<String>> result;
        try {
            List<String> scriptContent = Files.readAllLines(Paths.get(filePath));
            result = Optional.of(scriptContent);
        } catch (IOException e) {
            log.error(ExceptionUtils.getStackTrace(e));
            result = Optional.empty();
        }
        return result;
    }
}
