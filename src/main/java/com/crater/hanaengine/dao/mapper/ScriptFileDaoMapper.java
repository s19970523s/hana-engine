package com.crater.hanaengine.dao.mapper;

import com.crater.hanaengine.bean.db.ScriptTablePo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ScriptFileDaoMapper {
    int insert(ScriptTablePo scriptTablePo);
    int updateById(ScriptTablePo scriptTablePos);
    int updateBySaveId(ScriptTablePo scriptTablePo);
    int deleteById(ScriptTablePo scriptTablePo);
    ScriptTablePo queryById(ScriptTablePo scriptTablePo);
    List<ScriptTablePo> queryAll();
    List<ScriptTablePo> queryBySaveId(ScriptTablePo scriptTablePo);
}
