package com.crater.hanaengine.dao.mapper;

import com.crater.hanaengine.bean.db.SaveTablePo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface QuickSaveDaoMapper {
    int insert(SaveTablePo saveTablePo);
    int updateById(SaveTablePo saveTablePo);
    int deleteById(SaveTablePo saveTablePo);
    SaveTablePo queryById(SaveTablePo saveTablePo);
    List<SaveTablePo> queryAll();
}
