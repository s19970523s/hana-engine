package com.crater.hanaengine.dao.mapper;

import com.crater.hanaengine.bean.db.FlagTablePo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface FlagDaoMapper {
    int insert(FlagTablePo flagTablePo);
    int updateById(FlagTablePo flagTablePo);
    int deleteById(FlagTablePo flagTablePo);
    FlagTablePo queryById(FlagTablePo flagTablePo);
    FlagTablePo queryBySaveIdAndFlagName(FlagTablePo flagTablePo);
    List<FlagTablePo> queryAll();
}
