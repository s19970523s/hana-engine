package com.crater.hanaengine.dao;

import com.crater.hanaengine.bean.db.SaveTablePo;

import java.util.List;
import java.util.Optional;

public interface SaveDao {
    void insertData(SaveTablePo saveTablePo);
    boolean deleteData(Long targetId);
    Optional<SaveTablePo> querySaveData(Long targetSaveId);
    Optional<List<SaveTablePo>> queryAllSaveData();
    void updateData(SaveTablePo saveTablePo);
}
