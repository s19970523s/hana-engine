package com.crater.hanaengine.dao;

import com.crater.hanaengine.bean.db.ScriptTablePo;

import java.util.List;

public interface ScriptFileDao {
    List<ScriptTablePo> queryAll();
    void insertData(ScriptTablePo scriptTablePo);
    boolean deleteData(Long targetId);
    List<ScriptTablePo> queryScriptData(Long targetSaveId);
    void updateData(Long targetSaeId, ScriptTablePo scriptTablePo);
    void updateData(ScriptTablePo scriptTablePo);
}
