package com.crater.hanaengine.dao.impl;

import com.crater.hanaengine.bean.db.SaveTablePo;
import com.crater.hanaengine.dao.SaveDao;
import com.crater.hanaengine.dao.mapper.SaveDaoMapper;
import com.crater.hanaengine.exception.HanaDaoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class SaveDaoImpl implements SaveDao {
    private SaveDaoMapper saveDaoMapper;
    @Override
    public void insertData(SaveTablePo saveTablePo) {
        try {
            var changeRowCount = saveDaoMapper.insert(saveTablePo);
            if (changeRowCount == 0) throw new HanaDaoException("not insert save data");
        } catch (Exception e) {
            throw new HanaDaoException("insert save data fail", e);
        }
    }

    @Override
    public boolean deleteData(Long targetId) {
        return false;
    }

    @Override
    public Optional<SaveTablePo> querySaveData(Long targetSaveId) {
        try {
            var queryData = new SaveTablePo(targetSaveId, null, null, null,
                    null, null, null, null, null,
                    null, null, null);
            var saveTablePo = saveDaoMapper.queryById(queryData);
            return Optional.ofNullable(saveTablePo);
        } catch (Exception e) {
            throw new HanaDaoException("query save data fail", e);
        }
    }

    @Override
    public Optional<List<SaveTablePo>> queryAllSaveData() {
        try {
            var result = saveDaoMapper.queryAll();
            if (result.size() == 0) result = null;
            return Optional.ofNullable(result);
        } catch (Exception e) {
            throw new HanaDaoException("query all save data fail", e);
        }
    }

    @Override
    public void updateData(SaveTablePo saveTablePo) {
        try {
            var changeRowCount = saveDaoMapper.updateById(saveTablePo);
            if (changeRowCount == 0) throw new HanaDaoException("not update anything for save table");
        } catch (Exception e) {
            throw new HanaDaoException("update save table fail", e);
        }
    }

    @Autowired
    public SaveDaoImpl setSaveDaoMapper(SaveDaoMapper saveDaoMapper) {
        this.saveDaoMapper = saveDaoMapper;
        return this;
    }
}
