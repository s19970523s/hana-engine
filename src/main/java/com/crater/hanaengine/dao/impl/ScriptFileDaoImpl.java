package com.crater.hanaengine.dao.impl;

import com.crater.hanaengine.bean.db.ScriptTablePo;
import com.crater.hanaengine.dao.ScriptFileDao;
import com.crater.hanaengine.dao.mapper.ScriptFileDaoMapper;
import com.crater.hanaengine.exception.HanaDaoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ScriptFileDaoImpl implements ScriptFileDao {
    private ScriptFileDaoMapper scriptFileDaoMapper;

    @Override
    public List<ScriptTablePo> queryAll() {
        try {
            return scriptFileDaoMapper.queryAll();
        } catch (Exception e) {
            throw new HanaDaoException("query all script data fail", e);
        }
    }

    @Override
    public void insertData(ScriptTablePo scriptTablePo) {
        try {
            var changeRowCount = scriptFileDaoMapper.insert(scriptTablePo);
            if (changeRowCount == 0) throw new HanaDaoException("not insert script data");
        } catch (Exception e) {
            throw new HanaDaoException("insert script data fail", e);
        }
    }

    @Override
    public boolean deleteData(Long targetId) {
        return false;
    }

    @Override
    public List<ScriptTablePo> queryScriptData(Long targetSaveId) {
        try {
            var queryData = new ScriptTablePo().setSaveId(targetSaveId);
            return scriptFileDaoMapper.queryBySaveId(queryData);
        } catch (Exception e) {
            throw new HanaDaoException("query script data fail", e);
        }
    }

    @Override
    public void updateData(Long targetSaveId, ScriptTablePo scriptTablePo) {
        try {
            var updateData = new ScriptTablePo(scriptTablePo);
            updateData.setSaveId(targetSaveId);
            var changeRowCount = scriptFileDaoMapper.updateBySaveId(updateData);
            if (changeRowCount == 0) throw new HanaDaoException("not update anything for script table");
        } catch (Exception e) {
            throw new HanaDaoException("update script table fail", e);
        }
    }

    @Override
    public void updateData(ScriptTablePo scriptTablePo) {
        try {
            var updateData = new ScriptTablePo(scriptTablePo);
            var changeRowCount = scriptFileDaoMapper.updateById(updateData);
            if (changeRowCount == 0) throw new HanaDaoException("not update anything for script table");
        } catch (Exception e) {
            throw new HanaDaoException("update script table fail", e);
        }
    }

    @Autowired
    public ScriptFileDaoImpl setScriptFileDaoMapper(ScriptFileDaoMapper scriptFileDaoMapper) {
        this.scriptFileDaoMapper = scriptFileDaoMapper;
        return this;
    }
}
