package com.crater.hanaengine.dao.impl;

import com.crater.hanaengine.bean.db.FlagTablePo;
import com.crater.hanaengine.dao.FlagDao;
import com.crater.hanaengine.dao.mapper.FlagDaoMapper;
import com.crater.hanaengine.exception.HanaDaoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class FlagDaoImpl implements FlagDao {
    private FlagDaoMapper flagDaoMapper;

    @Override
    public boolean insertData(FlagTablePo flagTablePo) {
        try {
            int updateRow = flagDaoMapper.insert(flagTablePo);
            if (updateRow == 0) throw new HanaDaoException("insert flag fail");
            else return true;
        } catch (HanaDaoException e) {
            throw e;
        } catch (Exception e) {
            throw new HanaDaoException("insert flag fail", e);
        }
    }

    @Override
    public boolean deleteData(Long targetId) {
        return false;
    }

    @Override
    public Optional<FlagTablePo> queryFlag(Long saveId, String flagName) {
        return Optional.empty();
    }

    @Override
    public Optional<Boolean> queryFlagIsStand(Long saveId, String flagName) {
        return Optional.empty();
    }

    @Override
    public boolean updateFlagStand(Long targetSaveId, String targetFlagName, boolean isStand) {
        return false;
    }

    @Override
    public boolean updateFlagTable(Long targetFlagId, FlagTablePo updateData) {
        return false;
    }

    @Autowired
    public FlagDaoImpl setFlagDaoMapper(FlagDaoMapper flagDaoMapper) {
        this.flagDaoMapper = flagDaoMapper;
        return this;
    }
}
