package com.crater.hanaengine.dao;

import com.crater.hanaengine.bean.db.SaveTablePo;

import java.util.Optional;

public interface QuickSaveDao {
    boolean insertData(SaveTablePo saveTablePo);
    boolean deleteData(Long targetId);
    Optional<SaveTablePo> querySaveData(Long targetSaveId);
    boolean updateQuerySaveData(Long targetSaeId, SaveTablePo saveTablePo);
}
