package com.crater.hanaengine.dao;

import com.crater.hanaengine.bean.db.FlagTablePo;

import java.util.Optional;

public interface FlagDao {
    boolean insertData(FlagTablePo flagTablePo);
    boolean deleteData(Long targetId);
    Optional<FlagTablePo> queryFlag(Long saveId, String flagName);
    Optional<Boolean> queryFlagIsStand(Long saveId, String flagName);
    boolean updateFlagStand(Long targetSaveId, String targetFlagName, boolean isStand);
    boolean updateFlagTable(Long targetFlagId, FlagTablePo updateData);
}
