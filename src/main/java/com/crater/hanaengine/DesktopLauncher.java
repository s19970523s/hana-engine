package com.crater.hanaengine;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.crater.hanaengine.view.HanaGame;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DesktopLauncher {
    private static final Logger log = LoggerFactory.getLogger(HanaEngineApplication.class);

    public static void main(String[] arg) {
        try {
            Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
            config.setWindowedMode(1280, 720);
            new Lwjgl3Application(new HanaGame(), config);
        } catch (RuntimeException e) {
            log.error(ExceptionUtils.getStackTrace(e));
            throw e;
        }
    }
}
