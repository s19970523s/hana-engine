package com.crater.hanaengine.bean.dto;

import java.util.List;
import java.util.Optional;

public class ScriptDto {
    private String scriptName;
    private Integer scriptFileStartNumber;
    private Integer scriptFileEndLineNumber;
    private String scriptContent;
    private List<String> scriptInstruction;
    private boolean isBottomOfFile;

    public String getScriptName() {
        return scriptName;
    }

    public ScriptDto setScriptName(String scriptName) {
        this.scriptName = scriptName;
        return this;
    }

    public Integer getScriptFileStartNumber() {
        return scriptFileStartNumber;
    }

    public ScriptDto setScriptFileStartLineNumber(Integer scriptFileStartNumber) {
        this.scriptFileStartNumber = scriptFileStartNumber;
        return this;
    }

    public Integer getScriptFileEndLineNumber() {
        return scriptFileEndLineNumber;
    }

    public ScriptDto setScriptFileEndLineNumber(Integer scriptFileEndLineNumber) {
        this.scriptFileEndLineNumber = scriptFileEndLineNumber;
        return this;
    }

    public Optional<String> getScriptContent() {
        return Optional.ofNullable(scriptContent);
    }

    public ScriptDto setScriptContent(String scriptContent) {
        this.scriptContent = scriptContent;
        return this;
    }

    public Optional<List<String>> getScriptInstruction() {
        return Optional.ofNullable(scriptInstruction);
    }

    public ScriptDto setScriptInstruction(List<String> scriptInstruction) {
        this.scriptInstruction = scriptInstruction;
        return this;
    }

    public boolean isBottomOfFile() {
        return isBottomOfFile;
    }

    public ScriptDto setBottomOfFile(boolean bottomOfFile) {
        isBottomOfFile = bottomOfFile;
        return this;
    }
}
