package com.crater.hanaengine.bean.dto;

public class RoleDto {
    private String fileName;
    private String cutscene;
    private String jumpAttachedComponentsItem;
    private String action;

    public String getFileName() {
        return fileName;
    }

    public RoleDto setFileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public String getCutscene() {
        return cutscene;
    }

    public RoleDto setCutscene(String cutscene) {
        this.cutscene = cutscene;
        return this;
    }

    public String getJumpAttachedComponentsItem() {
        return jumpAttachedComponentsItem;
    }

    public RoleDto setJumpAttachedComponentsItem(String jumpAttachedComponentsItem) {
        this.jumpAttachedComponentsItem = jumpAttachedComponentsItem;
        return this;
    }

    public String getAction() {
        return action;
    }

    public RoleDto setAction(String action) {
        this.action = action;
        return this;
    }
}
