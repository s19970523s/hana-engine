package com.crater.hanaengine.bean.dto;

public class ChatDto {
    private String userId;
    private String scriptFileName;
    private Long lineNumber;

    public String getUserId() {
        return userId;
    }

    public ChatDto setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public String getScriptFileName() {
        return scriptFileName;
    }

    public ChatDto setScriptFileName(String scriptFileName) {
        this.scriptFileName = scriptFileName;
        return this;
    }

    public Long getLineNumber() {
        return lineNumber;
    }

    public ChatDto setLineNumber(Long lineNumber) {
        this.lineNumber = lineNumber;
        return this;
    }
}
