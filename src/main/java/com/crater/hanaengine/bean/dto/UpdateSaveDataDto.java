package com.crater.hanaengine.bean.dto;

import java.time.LocalDateTime;

public record UpdateSaveDataDto(Long saveId, String remark, String currentPath, String backgroundFileName, String role1FileName,
                                String role2FileName, String role3FileName, String bgmFileName, String currentText,
                                LocalDateTime createDate, LocalDateTime updateDate) {
}
