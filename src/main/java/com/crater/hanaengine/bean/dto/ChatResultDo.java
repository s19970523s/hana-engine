package com.crater.hanaengine.bean.dto;

import java.util.Optional;

public class ChatResultDo {
    private String nextPath;
    private String cutscene;
    private String chatContent;
    private String backgroundImage;
    private String bgmFileName;
    private String soundEffect;
    private Optional<RoleDto> role1;
    private Optional<RoleDto> role2;
    private Optional<RoleDto> role3;

    public String getNextPath() {
        return nextPath;
    }

    public ChatResultDo setNextPath(String nextPath) {
        this.nextPath = nextPath;
        return this;
    }

    public String getCutscene() {
        return cutscene;
    }

    public ChatResultDo setCutscene(String cutscene) {
        this.cutscene = cutscene;
        return this;
    }

    public String getChatContent() {
        return chatContent;
    }

    public ChatResultDo setChatContent(String chatContent) {
        this.chatContent = chatContent;
        return this;
    }

    public String getBackgroundImage() {
        return backgroundImage;
    }

    public ChatResultDo setBackgroundImage(String backgroundImage) {
        this.backgroundImage = backgroundImage;
        return this;
    }

    public String getBgmFileName() {
        return bgmFileName;
    }

    public ChatResultDo setBgmFileName(String bgmFileName) {
        this.bgmFileName = bgmFileName;
        return this;
    }

    public String getSoundEffect() {
        return soundEffect;
    }

    public ChatResultDo setSoundEffect(String soundEffect) {
        this.soundEffect = soundEffect;
        return this;
    }

    public Optional<RoleDto> getRole1() {
        return role1;
    }

    public ChatResultDo setRole1(Optional<RoleDto> role1) {
        this.role1 = role1;
        return this;
    }

    public Optional<RoleDto> getRole2() {
        return role2;
    }

    public ChatResultDo setRole2(Optional<RoleDto> role2) {
        this.role2 = role2;
        return this;
    }

    public Optional<RoleDto> getRole3() {
        return role3;
    }

    public ChatResultDo setRole3(Optional<RoleDto> role3) {
        this.role3 = role3;
        return this;
    }
}
