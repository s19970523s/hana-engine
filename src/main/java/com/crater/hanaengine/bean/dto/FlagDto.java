package com.crater.hanaengine.bean.dto;

public class FlagDto {
    private Long saveId;
    private String flagName;
    private boolean isStand;

    public Long getSaveId() {
        return saveId;
    }

    public FlagDto setSaveId(Long saveId) {
        this.saveId = saveId;
        return this;
    }

    public String getFlagName() {
        return flagName;
    }

    public FlagDto setFlagName(String flagName) {
        this.flagName = flagName;
        return this;
    }

    public boolean isStand() {
        return isStand;
    }

    public FlagDto setStand(boolean stand) {
        isStand = stand;
        return this;
    }
}
