package com.crater.hanaengine.bean.dto;

import com.crater.hanaengine.enums.Level;
import com.crater.hanaengine.enums.Function;

public class InstructionDto {
    private Level level;
    private Function function;
    private String value;

    public Level getLevel() {
        return level;
    }

    public InstructionDto setLevel(Level level) {
        this.level = level;
        return this;
    }

    public Function getFunction() {
        return function;
    }

    public InstructionDto setFunction(Function function) {
        this.function = function;
        return this;
    }

    public String getValue() {
        return value;
    }

    public InstructionDto setValue(String value) {
        this.value = value;
        return this;
    }
}
