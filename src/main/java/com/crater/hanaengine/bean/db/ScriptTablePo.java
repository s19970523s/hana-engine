package com.crater.hanaengine.bean.db;

import java.time.LocalDateTime;

public class ScriptTablePo {
    private Long id;
    private Long saveId;
    private String scriptFileName;
    private Long scriptFileLineNumber;
    private LocalDateTime createDate;

    public ScriptTablePo() {

    }

    public ScriptTablePo(ScriptTablePo scriptTablePo) {
        this.id = scriptTablePo.getId();
        this.saveId = scriptTablePo.getSaveId();
        this.scriptFileName = scriptTablePo.getScriptFileName();
        this.scriptFileLineNumber = scriptTablePo.getScriptFileLineNumber();
        this.createDate = scriptTablePo.getCreateDate();
    }

    public Long getId() {
        return id;
    }

    public ScriptTablePo setId(Long id) {
        this.id = id;
        return this;
    }

    public Long getSaveId() {
        return saveId;
    }

    public ScriptTablePo setSaveId(Long saveId) {
        this.saveId = saveId;
        return this;
    }

    public String getScriptFileName() {
        return scriptFileName;
    }

    public ScriptTablePo setScriptFileName(String scriptFileName) {
        this.scriptFileName = scriptFileName;
        return this;
    }

    public Long getScriptFileLineNumber() {
        return scriptFileLineNumber;
    }

    public ScriptTablePo setScriptFileLineNumber(Long scriptFileLineNumber) {
        this.scriptFileLineNumber = scriptFileLineNumber;
        return this;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public ScriptTablePo setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
        return this;
    }
}
