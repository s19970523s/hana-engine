package com.crater.hanaengine.bean.db;

import com.crater.hanaengine.bean.dto.UpdateSaveDataDto;

import java.time.LocalDateTime;

public record SaveTablePo (Long id, String remarks, String scriptFileName, Long scriptFileLineNumber,
                           String backgroundFileName, String role1FileName, String role2FileName, String role3FileName,
                           String bgmFileName, String currentText, LocalDateTime createDate, LocalDateTime updateDate) {
    public static SaveTablePo converter2SaveTablePo(UpdateSaveDataDto updateSaveDataDto) {
        var path = updateSaveDataDto.currentPath().replaceAll("^/", "");
        var scriptFileName = path.split("/")[0];
        var scriptFileLineNumber = path.split("/")[1];
        return new SaveTablePo(updateSaveDataDto.saveId(), updateSaveDataDto.remark(), scriptFileName,
                (long) Integer.parseInt(scriptFileLineNumber), updateSaveDataDto.backgroundFileName(),
                updateSaveDataDto.role1FileName(), updateSaveDataDto.role2FileName(), updateSaveDataDto.role3FileName(),
                updateSaveDataDto.bgmFileName(), updateSaveDataDto.currentText(), updateSaveDataDto.createDate(), updateSaveDataDto.updateDate());
    }
}
