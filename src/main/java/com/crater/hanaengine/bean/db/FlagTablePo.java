package com.crater.hanaengine.bean.db;

import java.time.LocalDate;

public class FlagTablePo {
    private Long id;
    private Long saveId;
    private String flagName;
    private boolean isStand;
    private LocalDate createDate;

    public Long getId() {
        return id;
    }

    public FlagTablePo setId(Long id) {
        this.id = id;
        return this;
    }

    public Long getSaveId() {
        return saveId;
    }

    public FlagTablePo setSaveId(Long saveId) {
        this.saveId = saveId;
        return this;
    }

    public String getFlagName() {
        return flagName;
    }

    public FlagTablePo setFlagName(String flagName) {
        this.flagName = flagName;
        return this;
    }

    public boolean isStand() {
        return isStand;
    }

    public FlagTablePo setStand(boolean stand) {
        isStand = stand;
        return this;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public FlagTablePo setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
        return this;
    }
}
