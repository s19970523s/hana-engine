package com.crater.hanaengine.bean.response.saveData;

public record SaveData(Long saveId, String remark, String currentPath, String nextPath, String backgroundFileName,
                       String role1FileName, String role2FileName, String role3FileName, String bgmFileName,
                       String currentText, String updateDate) {
}
