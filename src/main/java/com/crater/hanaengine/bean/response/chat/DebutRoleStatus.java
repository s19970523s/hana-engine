package com.crater.hanaengine.bean.response.chat;

public class DebutRoleStatus {
    private String fileName;
    private String cutscene;
    private String jumpAttachedComponentsItem;
    private String action;

    public String getFileName() {
        return fileName;
    }

    public DebutRoleStatus setFileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public String getCutscene() {
        return cutscene;
    }

    public DebutRoleStatus setCutscene(String cutscene) {
        this.cutscene = cutscene;
        return this;
    }

    public String getJumpAttachedComponentsItem() {
        return jumpAttachedComponentsItem;
    }

    public DebutRoleStatus setJumpAttachedComponentsItem(String jumpAttachedComponentsItem) {
        this.jumpAttachedComponentsItem = jumpAttachedComponentsItem;
        return this;
    }

    public String getAction() {
        return action;
    }

    public DebutRoleStatus setAction(String action) {
        this.action = action;
        return this;
    }
}
