package com.crater.hanaengine.bean.response.saveData;

import com.crater.hanaengine.bean.response.BaseResponse;

import java.util.List;

public class GetSaveDataResponse extends BaseResponse {
    List<SaveData> saveData;

    public List<SaveData> getSaveData() {
        return saveData;
    }

    public GetSaveDataResponse setSaveData(List<SaveData> saveData) {
        this.saveData = saveData;
        return this;
    }
}
