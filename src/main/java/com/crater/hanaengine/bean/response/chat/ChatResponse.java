package com.crater.hanaengine.bean.response.chat;

import com.crater.hanaengine.bean.response.BaseResponse;

public class ChatResponse extends BaseResponse {
    private String nextPath;
    private String cutscene;
    private String chatContent;
    private String backgroundImage;
    private DebutRoleStatus role1;
    private DebutRoleStatus role2;
    private DebutRoleStatus role3;
    private String bgm;
    private String soundEffect;

    public String getNextPath() {
        return nextPath;
    }

    public ChatResponse setNextPath(String nextPath) {
        this.nextPath = nextPath;
        return this;
    }

    public String getCutscene() {
        return cutscene;
    }

    public ChatResponse setCutscene(String cutscene) {
        this.cutscene = cutscene;
        return this;
    }

    public String getChatContent() {
        return chatContent;
    }

    public ChatResponse setChatContent(String chatContent) {
        this.chatContent = chatContent;
        return this;
    }

    public String getBackgroundImage() {
        return backgroundImage;
    }

    public ChatResponse setBackgroundImage(String backgroundImage) {
        this.backgroundImage = backgroundImage;
        return this;
    }

    public DebutRoleStatus getRole1() {
        return role1;
    }

    public ChatResponse setRole1(DebutRoleStatus role1) {
        this.role1 = role1;
        return this;
    }

    public DebutRoleStatus getRole2() {
        return role2;
    }

    public ChatResponse setRole2(DebutRoleStatus role2) {
        this.role2 = role2;
        return this;
    }

    public DebutRoleStatus getRole3() {
        return role3;
    }

    public ChatResponse setRole3(DebutRoleStatus role3) {
        this.role3 = role3;
        return this;
    }

    public String getBgm() {
        return bgm;
    }

    public ChatResponse setBgm(String bgm) {
        this.bgm = bgm;
        return this;
    }

    public String getSoundEffect() {
        return soundEffect;
    }

    public ChatResponse setSoundEffect(String soundEffect) {
        this.soundEffect = soundEffect;
        return this;
    }
}
