package com.crater.hanaengine.bean.response.saveData;

public class SaveDataRequest {
    private Long saveId;
    private String currentUrlPath;
    private String remark;
    private String backgroundFileName;
    private String role1FileName;
    private String role2FileName;
    private String role3FileName;
    private String bgmFileName;
    private String currentText;

    public Long getSaveId() {
        return saveId;
    }

    public SaveDataRequest setSaveId(Long saveId) {
        this.saveId = saveId;
        return this;
    }

    public String getCurrentUrlPath() {
        return currentUrlPath;
    }

    public SaveDataRequest setCurrentUrlPath(String currentUrlPath) {
        this.currentUrlPath = currentUrlPath;
        return this;
    }

    public String getRemark() {
        return remark;
    }

    public SaveDataRequest setRemark(String remark) {
        this.remark = remark;
        return this;
    }

    public String getBackgroundFileName() {
        return backgroundFileName;
    }

    public SaveDataRequest setBackgroundFileName(String backgroundFileName) {
        this.backgroundFileName = backgroundFileName;
        return this;
    }

    public String getRole1FileName() {
        return role1FileName;
    }

    public SaveDataRequest setRole1FileName(String role1FileName) {
        this.role1FileName = role1FileName;
        return this;
    }

    public String getRole2FileName() {
        return role2FileName;
    }

    public SaveDataRequest setRole2FileName(String role2FileName) {
        this.role2FileName = role2FileName;
        return this;
    }

    public String getRole3FileName() {
        return role3FileName;
    }

    public SaveDataRequest setRole3FileName(String role3FileName) {
        this.role3FileName = role3FileName;
        return this;
    }

    public String getBgmFileName() {
        return bgmFileName;
    }

    public SaveDataRequest setBgmFileName(String bgmFileName) {
        this.bgmFileName = bgmFileName;
        return this;
    }

    public String getCurrentText() {
        return currentText;
    }

    public SaveDataRequest setCurrentText(String currentText) {
        this.currentText = currentText;
        return this;
    }
}
