package com.crater.hanaengine.bean.configurationProperties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "script")
public class ScriptProperties {
    private String scriptFolderName;

    public String getScriptFolderName() {
        return scriptFolderName;
    }

    public ScriptProperties setScriptFolderName(String scriptFolderName) {
        this.scriptFolderName = scriptFolderName;
        return this;
    }
}
