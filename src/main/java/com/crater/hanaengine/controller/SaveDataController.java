package com.crater.hanaengine.controller;

import com.crater.hanaengine.bean.dto.GetSaveDataDto;
import com.crater.hanaengine.bean.dto.UpdateSaveDataDto;
import com.crater.hanaengine.bean.response.saveData.GetSaveDataResponse;
import com.crater.hanaengine.bean.response.saveData.SaveDataRequest;
import com.crater.hanaengine.bean.response.saveData.NewSaveDataResponse;
import com.crater.hanaengine.bean.response.saveData.SaveData;
import com.crater.hanaengine.service.SaveDataService;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.stream.Collectors;

@RestController
public class SaveDataController {
    private SaveDataService saveDataService;
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @RequestMapping(value = "/saveData/getList", method = RequestMethod.GET)
    public GetSaveDataResponse getSaveList() {
        try {
            var saveInfo = saveDataService.queryAllSaveData();
            var saveDatas = saveInfo.parallelStream().map(this::generateGetSaveDataResponse)
                    .collect(Collectors.toList());
            var response = new GetSaveDataResponse();
            response.setErrorCode("0");
            return response.setSaveData(saveDatas);
        } catch (Exception e) {
            log.error(ExceptionUtils.getStackTrace(e));
            throw e;
        }
    }

    private SaveData generateGetSaveDataResponse(GetSaveDataDto updateSaveDataDto) {
        return new SaveData(updateSaveDataDto.saveId(), updateSaveDataDto.remark(), updateSaveDataDto.currentPath(),
                updateSaveDataDto.nextPath(),
                updateSaveDataDto.backgroundFileName(), updateSaveDataDto.role1FileName(), updateSaveDataDto.role2FileName(),
                updateSaveDataDto.role3FileName(), updateSaveDataDto.bgmFileName(), updateSaveDataDto.currentText(),
                updateSaveDataDto.updateDate().format(DateTimeFormatter.ISO_DATE));
    }

    @RequestMapping(value = "/saveData/new", method = RequestMethod.POST)
    public NewSaveDataResponse newSaveData(@RequestBody SaveDataRequest request) {
        var newSaveDataDto = generateSaveDataDto(request);
        try {
            saveDataService.newSaveData(newSaveDataDto);
            var response = new NewSaveDataResponse();
            response.setErrorCode("0");
            return response;
        } catch (Exception e) {
            log.error(ExceptionUtils.getStackTrace(e));
            var response = new NewSaveDataResponse();
            response.setErrorMessage("create new save data fail\n" + ExceptionUtils.getStackTrace(e));
            return response;
        }
    }

    @RequestMapping(value = "/saveData/update", method = RequestMethod.POST)
    public NewSaveDataResponse updateSaveData(@RequestBody SaveDataRequest request) {
        var saveDataDto = generateSaveDataDto(request);
        try {
            saveDataService.updateSaveData(saveDataDto);
            var response = new NewSaveDataResponse();
            response.setErrorCode("0");
            return response;
        } catch (Exception e) {
            log.error(ExceptionUtils.getStackTrace(e));
            var response = new NewSaveDataResponse();
            response.setErrorMessage("create new save data fail\n" + ExceptionUtils.getStackTrace(e));
            return response;
        }
    }

    private UpdateSaveDataDto generateSaveDataDto(SaveDataRequest request) {
        return new UpdateSaveDataDto(request.getSaveId(), request.getRemark(), request.getCurrentUrlPath(),
                request.getBackgroundFileName(), request.getRole1FileName(), request.getRole2FileName(),
                request.getRole3FileName(), request.getBgmFileName(), request.getCurrentText(), LocalDateTime.now(),
                LocalDateTime.now());
    }

    @Autowired
    public SaveDataController setSaveDataService(SaveDataService saveDataService) {
        this.saveDataService = saveDataService;
        return this;
    }
}
