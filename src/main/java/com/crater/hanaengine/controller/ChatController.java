package com.crater.hanaengine.controller;

import com.crater.hanaengine.bean.dto.ChatDto;
import com.crater.hanaengine.bean.dto.ChatResultDo;
import com.crater.hanaengine.bean.dto.RoleDto;
import com.crater.hanaengine.bean.request.RequestApiHeader;
import com.crater.hanaengine.bean.response.chat.ChatResponse;
import com.crater.hanaengine.bean.response.chat.DebutRoleStatus;
import com.crater.hanaengine.exception.HanaApiException;
import com.crater.hanaengine.service.ChatService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class ChatController {
    private ChatService chatService;
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @RequestMapping(value = "/chat/{scriptFileName}/{lineNumber}", method = RequestMethod.GET)
    public ChatResponse getChat(@RequestHeader(RequestApiHeader.SAVE_ID) String userId,
                                @PathVariable("scriptFileName") String scriptFileName,
                                @PathVariable("lineNumber") Long lineNumber) {
        try {
            checkIsChatRequestLegal(userId, scriptFileName, lineNumber);
            var chatDto = generateChatDto(userId, scriptFileName, lineNumber);
            var chatResultDto = takeChatContent(chatDto);
            return generateChatResponse(chatResultDto);
        } catch (HanaApiException e) {
            log.error(ExceptionUtils.getStackTrace(e));
            var response = new ChatResponse();
            response.setErrorMessage(e.getMessage());
            return response;
        } catch (Exception e) {
            log.error(ExceptionUtils.getStackTrace(e));
            var response = new ChatResponse();
            response.setErrorMessage("get chat have unknown error");
            return response;
        }
    }

    private void checkIsChatRequestLegal(String userID, String scriptFileName, Long lineNumber) {
        var errorMessage = new StringBuilder();
        if (StringUtils.isEmpty(userID)) errorMessage.append("userId ");
        if (StringUtils.isEmpty(scriptFileName)) errorMessage.append("scriptFileName ");
        if (lineNumber == null) errorMessage.append("lineNumber ");
        if (errorMessage.length() > 0) throw new HanaApiException(errorMessage.append("can not be null").toString());
    }

    private ChatDto generateChatDto(String userId, String scriptFileName, Long lineNumber) {
        try {
            return new ChatDto().setUserId(userId).setScriptFileName(scriptFileName).setLineNumber(lineNumber);
        } catch (Exception e) {
            throw new HanaApiException("generate chatDto have error, please check request.", e);
        }
    }

    private ChatResultDo takeChatContent(ChatDto chatDto) {
        try {
            return chatService.takeChat(chatDto);
        } catch (Exception e) {
            throw new HanaApiException("call chat service have unknown error", e);
        }
    }

    private ChatResponse generateChatResponse(ChatResultDo chatResultDo) {
        try {
            var role1 = chatResultDo.getRole1().isPresent() ? chateDto2DebutRoleStatus(chatResultDo.getRole1().get()) : null;
            var role2 = chatResultDo.getRole2().isPresent() ? chateDto2DebutRoleStatus(chatResultDo.getRole2().get()) : null;
            var role3 = chatResultDo.getRole3().isPresent() ? chateDto2DebutRoleStatus(chatResultDo.getRole3().get()) : null;
            return new ChatResponse().setNextPath(chatResultDo.getNextPath()).setCutscene(chatResultDo.getCutscene())
                    .setChatContent(chatResultDo.getChatContent()).setBackgroundImage(chatResultDo.getBackgroundImage())
                    .setRole1(role1).setRole2(role2).setRole3(role3).setBgm(chatResultDo.getBgmFileName())
                    .setSoundEffect(chatResultDo.getSoundEffect());
        } catch (Exception e) {
            throw new HanaApiException("generate chat response have unknown error", e);
        }
    }

    private DebutRoleStatus chateDto2DebutRoleStatus(RoleDto roleDto) {
        try {
            return new DebutRoleStatus().setFileName(roleDto.getFileName()).setCutscene(roleDto.getCutscene())
                    .setJumpAttachedComponentsItem(roleDto.getJumpAttachedComponentsItem())
                    .setAction(roleDto.getAction());
        } catch (Exception e) {
            throw new HanaApiException("generate DebutRoleStatus have unknown error", e);
        }
    }

    @Autowired
    public ChatController setChatService(ChatService chatService) {
        this.chatService = chatService;
        return this;
    }
}
