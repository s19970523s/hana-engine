package com.crater.hanaengine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class HanaEngineApplication {
    private static ConfigurableApplicationContext context;
    private static final Logger log = LoggerFactory.getLogger(HanaEngineApplication.class);

    public static void main(String[] args) {
        log.info("start Hana engin");
        context = SpringApplication.run(HanaEngineApplication.class, args);
        log.info("will run DesktopLauncher");
        DesktopLauncher.main(null);
    }

    public static void exitApplication() {
        log.info("app will close");
        int exitCode = SpringApplication.exit(context, () -> 0);
        System.exit(exitCode);
    }

}
