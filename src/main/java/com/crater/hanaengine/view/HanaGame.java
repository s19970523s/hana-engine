package com.crater.hanaengine.view;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.crater.hanaengine.HanaEngineApplication;
import com.crater.hanaengine.view.mainMenu.MainMenuScreen;
import com.crater.hanaengine.view.model.MouseInfo;
import com.crater.hanaengine.view.utils.PathUtils;

import java.io.File;

public class HanaGame extends Game {
    public SpriteBatch batch;
    public BitmapFont font;
    public BitmapFont logFont;
    public MouseInfo mouseInfo;

    @Override
    public void create() {
        settingBatch();
        settingFont();
        settingLogFont();
        settingMouseInfo();
        this.setScreen(new MainMenuScreen(this));
    }

    private void settingBatch() {
        batch = new SpriteBatch();
    }

    private void settingFont() {
        font = new BitmapFont(new FileHandle(new File(
                PathUtils.generateFontPath("NotoSansTC-Black.fnt"))), false);
        font.setColor(Color.WHITE);
        font.getData().setScale(2);
    }

    private void settingLogFont() {
        logFont = new BitmapFont(new FileHandle(new File(
                PathUtils.generateFontPath("NotoSansTC-Black.fnt"))), false);
        logFont.setColor(Color.BLACK);
        logFont.getData().setScale(2);
    }

    private void settingMouseInfo() {
        mouseInfo = new MouseInfo();
    }

    public void render() {
        super.render();
    }

    public void dispose() {
        batch.dispose();
        font.dispose();
        HanaEngineApplication.exitApplication();
    }
}
