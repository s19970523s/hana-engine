package com.crater.hanaengine.view.Dto;

import com.crater.hanaengine.view.chat.BackgroundImage;
import com.crater.hanaengine.view.chat.Role;
import com.crater.hanaengine.view.mainMenu.RoleMapKey;

import java.util.Map;

public record SaveDataDto(String currentApiPath, BackgroundImage backgroundImage, String bgmFileName,
                          Map<RoleMapKey, Role> roleMap, String contentText, String remark) {
}
