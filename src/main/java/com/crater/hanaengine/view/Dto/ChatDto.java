package com.crater.hanaengine.view.Dto;

public class ChatDto {
    private String nextPath;
    private Integer fileLineNumber;
    private String role1ImageName;
    private String role2ImageName;
    private String role3ImageName;
    private String content;
    private String backgroundImageName;
    private String soundEffect;
    private String bgm;

    public String getNextPath() {
        return nextPath;
    }

    public ChatDto setNextPath(String nextPath) {
        this.nextPath = nextPath;
        return this;
    }

    public Integer getFileLineNumber() {
        return fileLineNumber;
    }

    public ChatDto setFileLineNumber(Integer fileLineNumber) {
        this.fileLineNumber = fileLineNumber;
        return this;
    }

    public String getRole1ImageName() {
        return role1ImageName;
    }

    public ChatDto setRole1ImageName(String role1ImageName) {
        this.role1ImageName = role1ImageName;
        return this;
    }

    public String getRole2ImageName() {
        return role2ImageName;
    }

    public ChatDto setRole2ImageName(String role2ImageName) {
        this.role2ImageName = role2ImageName;
        return this;
    }

    public String getRole3ImageName() {
        return role3ImageName;
    }

    public ChatDto setRole3ImageName(String role3ImageName) {
        this.role3ImageName = role3ImageName;
        return this;
    }

    public String getContent() {
        return content;
    }

    public ChatDto setContent(String content) {
        this.content = content;
        return this;
    }

    public String getBackgroundImageName() {
        return backgroundImageName;
    }

    public ChatDto setBackgroundImageName(String backgroundImageName) {
        this.backgroundImageName = backgroundImageName;
        return this;
    }

    public String getSoundEffect() {
        return soundEffect;
    }

    public ChatDto setSoundEffect(String soundEffect) {
        this.soundEffect = soundEffect;
        return this;
    }

    public String getBgm() {
        return bgm;
    }

    public ChatDto setBgm(String bgm) {
        this.bgm = bgm;
        return this;
    }
}
