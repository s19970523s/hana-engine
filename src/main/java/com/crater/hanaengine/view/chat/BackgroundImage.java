package com.crater.hanaengine.view.chat;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.crater.hanaengine.view.utils.PathUtils;

import java.io.File;
import java.util.Objects;

public class BackgroundImage {
    private final Rectangle rectangle;
    private Texture texture;
    private boolean isShowOnScreen;
    private String imageFileName;
    private final static float PLACE_X = 0;
    private final static float PLACE_Y = 0;
    private final static float WIDTH = 1920;
    private final static float HEIGHT = 1080;


    public BackgroundImage() {
        rectangle = new Rectangle(PLACE_X, PLACE_Y, WIDTH, HEIGHT);
        isShowOnScreen = true;
    }

    public BackgroundImage settingImage(String imageName) {
        if (Objects.nonNull(imageName)) imageFileName = imageName;
        if ("null".equals(imageName)) texture = null;
        else if (imageName != null)
            texture = new Texture(new FileHandle(new File(PathUtils.generateImagePath(imageName))));
        return this;
    }

    public boolean checkIsShowOnScreen() {
        return (!(texture == null)); //背景不會被使用者關閉，所以不判斷isShowOnScreen
    }


    public float[] takePlacePoint() {
        return new float[]{PLACE_X, PLACE_Y};
    }

    public float[] takeSize() {
        return new float[]{WIDTH, HEIGHT};
    }

    public Rectangle getRoleRectangle() {
        return rectangle;
    }

    public Texture getTexture() {
        return texture;
    }

    public void settingIsShowOnScreen(boolean isShowOnScreen) {
        this.isShowOnScreen = isShowOnScreen;
    }

    public void draw(SpriteBatch batch) {
        batch.draw(getTexture(), PLACE_X, PLACE_Y, WIDTH, HEIGHT);
    }

    public void disposeTexture() {
        if (Objects.nonNull(texture)) texture.dispose();
    }

    public String getImageFileName() {
        return imageFileName;
    }
}
