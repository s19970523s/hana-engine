package com.crater.hanaengine.view.chat;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.crater.hanaengine.view.model.MouseInfo;
import com.crater.hanaengine.view.utils.PathUtils;

import java.io.File;
import java.util.Objects;

public class LoadButton implements ChatItem{
    private final Rectangle rectangle;
    private Texture texture;
    private boolean isShowOnScreen;
    private final static float PLACE_X = 220;
    private final static float PLACE_Y = 357;
    private final static float WIDTH = 80;
    private final static float HEIGHT = 60;


    public LoadButton() {
        rectangle = new Rectangle(PLACE_X, PLACE_Y, WIDTH, HEIGHT);
        isShowOnScreen = true;
    }

    @Override
    public ChatItem settingImage(String imageName) {
        if ("null".equals(imageName)) texture = null;
        else if (imageName != null)
            texture = new Texture(new FileHandle(new File(PathUtils.generateImagePath(imageName))));
        return this;
    }

    @Override
    public boolean checkIsShowOnScreen() {
        return (!(texture == null)) && isShowOnScreen;
    }


    @Override
    public float[] takePlacePoint() {
        return new float[]{PLACE_X, PLACE_Y};
    }

    @Override
    public float[] takeSize() {
        return new float[]{WIDTH, HEIGHT};
    }

    @Override
    public Rectangle getRectangle() {
        return rectangle;
    }

    @Override
    public Texture getTexture() {
        return texture;
    }

    @Override
    public void settingIsShowOnScreen(boolean isShowOnScreen) {
        this.isShowOnScreen = isShowOnScreen;
    }

    @Override
    public void draw(SpriteBatch batch) {
        batch.draw(getTexture(), PLACE_X, PLACE_Y, WIDTH, HEIGHT);
    }

    @Override
    public void disposeTexture() {
        if (Objects.nonNull(texture)) texture.dispose();
    }

    @Override
    public boolean isTouch(MouseInfo mouseInfo, OrthographicCamera camera) {
        return rectangle.overlaps(mouseInfo.getMouseRectangle(camera));
    }

    @Override
    public boolean isClick(MouseInfo mouseInfo, OrthographicCamera camera) {
        return mouseInfo.isLeftClick() && rectangle.overlaps(mouseInfo.getMouseRectangle(camera)) && isShowOnScreen;
    }
}
