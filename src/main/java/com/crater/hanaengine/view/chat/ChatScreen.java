package com.crater.hanaengine.view.chat;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.utils.ScreenUtils;
import com.badlogic.gdx.utils.TimeUtils;
import com.crater.hanaengine.view.Dto.ChatDto;
import com.crater.hanaengine.view.Dto.SaveDataDto;
import com.crater.hanaengine.view.HanaGame;
import com.crater.hanaengine.view.enums.OtherImageData;
import com.crater.hanaengine.view.mainMenu.RoleMapKey;
import com.crater.hanaengine.view.utils.FrontEndCommunication;
import com.crater.hanaengine.view.utils.PathUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.*;

public class ChatScreen implements Screen {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private final HanaGame hanaGame;
    private OrthographicCamera camera;
    private Map<RoleMapKey, Role> roleMap;
    private Map<OtherImageData, ChatItem> otherChatItemMap;
    private BackgroundImage backgroundImage;
    private String contentText;
    private String appearsText;
    private Long lastAppearsTextTime;
    private boolean isRunNextStepOver;
    private String userId;
    private String currentApiPath;
    private String nextChatApiPath;
    private Music bgm;
    private String bgmFileName;
    private boolean isShowItemAndContent = true;
    private static final boolean IS_SHOW_MOUSE_PLACE = true;
    private boolean isLoadSaveDataFromMainMenu;

    public ChatScreen(HanaGame hanaGame) {
        this.hanaGame = hanaGame;
    }

    public ChatScreen(HanaGame hanaGame, boolean isLoad) {
        this.hanaGame = hanaGame;
        isLoadSaveDataFromMainMenu = true;
    }

    @Override
    public void show() {
        log.info("show chat screen");
        try {
            nextChatApiPath = "/main.hana/0";
            userId = "test";
            appearsText = "";
            lastAppearsTextTime = TimeUtils.nanoTime();
            isRunNextStepOver = true;
            initCamera();
            initBackground();
            initOtherItem();
            initRole();
            doNextStep();
        } catch (RuntimeException e) {
            log.error("init chat screen fail", e);
            closeEngine();
        }
    }

    private void initRole() {
        try {
            roleMap = new HashMap<>();
            var role1 = new Role1();
            var role2 = new Role2();
            var role3 = new Role3();
            roleMap.put(RoleMapKey.role1, role1);
            roleMap.put(RoleMapKey.role2, role2);
            roleMap.put(RoleMapKey.role3, role3);
        } catch (Exception e) {
            throw new RuntimeException("init role fail", e);
        }
    }

    private void initOtherItem() {
        try {
            otherChatItemMap = new HashMap<>();
            otherChatItemMap.put(OtherImageData.dialogBox, new DialogBox().settingImage("dialogBox.png"));
            otherChatItemMap.put(OtherImageData.saveButton, new SaveButton().settingImage("saveForChat.png"));
            otherChatItemMap.put(OtherImageData.loadButton, new LoadButton().settingImage("continueForChat.png"));
            var isHaveSaveData = FrontEndCommunication.checkHaveSaveData(userId);
            if (!isHaveSaveData) otherChatItemMap.get(OtherImageData.loadButton).settingIsShowOnScreen(false);
        } catch (Exception e) {
            throw new RuntimeException("init other item fail", e);
        }
    }

    private void initCamera() {
        try {
            camera = new OrthographicCamera();
            camera.setToOrtho(false, 1920, 1080);
        } catch (Exception e) {
            throw new RuntimeException("init camera fail", e);
        }
    }

    private void initBackground() {
        try {
            backgroundImage = new BackgroundImage();
        } catch (Exception e) {
            throw new RuntimeException("init background fail", e);
        }
    }

    @Override
    public void render(float delta) {
        try {
            ScreenUtils.clear(0, 0, 0, 1);
            camera.update();
            hanaGame.batch.begin();
            drawBackgroundImage();
            drawRole(roleMap);
            drawOtherItem();
            drawContent(isShowItemAndContent);
            drawMousePlaceLog();
            hanaGame.batch.end();
            if (isLoadSaveDataFromMainMenu) {
                doLoadData();
                isLoadSaveDataFromMainMenu = !isLoadSaveDataFromMainMenu;
            }
            useKeyboardAndMouse();
            drawText();
        } catch (Exception e) {
            log.error(ExceptionUtils.getStackTrace(e));
            closeEngine();
        }
    }

    private void drawMousePlaceLog() {
        if (IS_SHOW_MOUSE_PLACE) {
            var mousePlace = hanaGame.mouseInfo.getCameraPlace(camera);
            hanaGame.logFont.draw(hanaGame.batch, "mouse x: " + mousePlace[0], 20, 1060);
            hanaGame.logFont.draw(hanaGame.batch, "mouse y: " + mousePlace[1], 20, 1000);
        }
    }

    private void drawRole(Map<RoleMapKey, Role> roleMap) {
        roleMap.forEach((k, v) -> {
            if (v.checkIsShowOnScreen()) v.draw(hanaGame.batch);
        });
    }

    private void drawBackgroundImage() {
        if (Objects.nonNull(backgroundImage) && backgroundImage.checkIsShowOnScreen())
            backgroundImage.draw(hanaGame.batch);
    }

    private void drawOtherItem() {
        var dialogBox = otherChatItemMap.get(OtherImageData.dialogBox);
        if (dialogBox.checkIsShowOnScreen()) dialogBox.draw(hanaGame.batch);
        otherChatItemMap.forEach((k, v) -> {
            if (v.checkIsShowOnScreen() && !k.equals(OtherImageData.dialogBox)) v.draw(hanaGame.batch);
        });
    }

    private void drawContent(boolean isShowItemAndContent) {
        if (isShowItemAndContent) {
            hanaGame.font.draw(hanaGame.batch, appearsText == null ? "" : appearsText, 100, 250);
        }
    }

    private void useKeyboardAndMouse() {
        if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
            isShowItemAndContent = !isShowItemAndContent;
            otherChatItemMap.forEach((k, v) -> {
                v.settingIsShowOnScreen(isShowItemAndContent);
                otherChatItemMap.put(k, v);
            });
        }
        if (Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT)) {
            if (!isRunNextStepOver && !StringUtils.equals(appearsText, contentText)) showAllContent();
            else doNextStep();
        }
        if (otherChatItemMap.get(OtherImageData.saveButton).isClick(hanaGame.mouseInfo, camera)) {
            doSaveData();
        } else if (otherChatItemMap.get(OtherImageData.loadButton).isClick(hanaGame.mouseInfo, camera)) {
            doLoadData();
        } else if (Gdx.input.isKeyJustPressed(Input.Keys.ENTER) || hanaGame.mouseInfo.isLeftJustPressedClick()) {
            if (!isRunNextStepOver && !StringUtils.equals(appearsText, contentText)) showAllContent();
            else doNextStep();
        }
    }

    private void doNextStep() {
        isRunNextStepOver = false;
        appearsText = "";
        if (StringUtils.isEmpty(nextChatApiPath)) {
            closeEngine();
            return;
        }
        var chatDto = FrontEndCommunication.callGetChat(nextChatApiPath, userId);
        settingContentText(chatDto);
        settingRole(chatDto);
        settingBackground(chatDto);
        settingBgm(chatDto);
        settingSoundEffect(chatDto);
        currentApiPath = nextChatApiPath;
        nextChatApiPath = chatDto.getNextPath();
    }

    private void showAllContent() {
        appearsText = contentText;
        isRunNextStepOver = true;
    }

    private void settingContentText(ChatDto chatDto) {
        contentText = chatDto.getContent();
        contentText = contentText.replaceAll("(.{1,30})", "$1\n");
        contentText = contentText.replaceAll("\n\n", "\n");
    }

    private void settingRole(ChatDto chatDto) {
        roleMap.put(RoleMapKey.role1, roleMap.get(RoleMapKey.role1).settingRoleImage(chatDto.getRole1ImageName()));
        roleMap.put(RoleMapKey.role2, roleMap.get(RoleMapKey.role2).settingRoleImage(chatDto.getRole2ImageName()));
        roleMap.put(RoleMapKey.role3, roleMap.get(RoleMapKey.role3).settingRoleImage(chatDto.getRole3ImageName()));
    }

    private void settingBackground(ChatDto chatDto) {
        backgroundImage = backgroundImage.settingImage(chatDto.getBackgroundImageName());
    }

    private void settingBgm(ChatDto chatDto) {
        if ("null".equals(chatDto.getBgm())) {
            if (Objects.nonNull(bgm) && bgm.isPlaying()) bgm.pause();
        } else if (Objects.nonNull(chatDto.getBgm())) {
            bgmFileName = chatDto.getBgm();
            Optional.ofNullable(bgm).ifPresent(Music::stop);
            bgm = Gdx.audio.newMusic(new FileHandle(new File(PathUtils.generateMusicPath(chatDto.getBgm()))));
            bgm.setLooping(true);
            bgm.setVolume(0.5F);
            bgm.play();
        }
    }

    private void settingSoundEffect(ChatDto chatDto) {
        if (StringUtils.isNoneEmpty(chatDto.getSoundEffect())) {
            var soundEffect =
                    Gdx.audio.newSound(new FileHandle(PathUtils.generateMusicPath(chatDto.getSoundEffect())));
            soundEffect.play();
        }
    }

    private void drawText() {
        if (!isRunNextStepOver && StringUtils.equals(contentText, appearsText)) {
            isRunNextStepOver = true;
        } else if (!isRunNextStepOver && StringUtils.isNoneEmpty(contentText) &&
                contentText.length() > appearsText.length() &&
                TimeUtils.nanoTime() - lastAppearsTextTime > 1000000L) {
            appearsText += contentText.charAt(appearsText.length());
            lastAppearsTextTime = TimeUtils.nanoTime();
        } else if (!StringUtils.equals(contentText, appearsText)) {
            appearsText = contentText;
        }
    }

    private void doLoadData() {
        try {
            log.info("will do load data.");
            var saveData = FrontEndCommunication.getSaveData(userId);
            settingContentText(saveData);
            settingRole(saveData);
            settingBackground(saveData);
            settingBgm(saveData);
            settingSoundEffect(saveData);
            isRunNextStepOver = false;
            currentApiPath = saveData.getNextPath();
            nextChatApiPath = saveData.getNextPath();
        } catch (Exception e) {
            throw new RuntimeException("do load data fail", e);
        }
    }

    private void doSaveData() {
        try {
            log.info("will do save data.");
            otherChatItemMap.get(OtherImageData.loadButton).settingIsShowOnScreen(true);
            FrontEndCommunication.saveData(new SaveDataDto(currentApiPath, backgroundImage, bgmFileName, roleMap,
                    contentText, "Quick Save"));
        } catch (Exception e) {
            throw new RuntimeException("save data fail", e);
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        backgroundImage.disposeTexture();
        roleMap.forEach((k, v) -> v.dispose());
        otherChatItemMap.forEach((k, v) -> v.disposeTexture());
        hanaGame.dispose();
    }

    private void closeEngine() {
        try {
            Optional.ofNullable(backgroundImage).ifPresent(BackgroundImage::disposeTexture);
            Optional.ofNullable(roleMap).ifPresent(r -> r.forEach((k, v) -> v.dispose()));
            Optional.ofNullable(otherChatItemMap).ifPresent(o -> o.forEach((k, v) -> v.disposeTexture()));
        } finally {
            hanaGame.dispose();
        }
    }
}
