package com.crater.hanaengine.view.chat;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public interface Role {
    Role settingRoleImage(String imageName);

    boolean checkIsShowOnScreen();

    /**
     * take role x and y
     * @return float[x, y]
     */
    float[] takePlacePoint();

    /**
     * take role image and rectangle size
     * @return float[width, height]
     */
    float[] takeSize();

    Rectangle getRoleRectangle();

    Texture getTexture();

    void draw(SpriteBatch batch);

    void dispose();

    String getImageName();
}
