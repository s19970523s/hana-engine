package com.crater.hanaengine.view.chat;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.crater.hanaengine.view.utils.PathUtils;

import java.io.File;
import java.util.Objects;

public class Role1 implements Role {
    private String imageFileName;
    private final Rectangle rectangle;
    private Texture texture;
    private final static float PLACE_X = -10;
    private final static float PLACE_Y = 0;
    private final static float WIDTH = 800;
    private final static float HEIGHT = 900;

    public Role1() {
        rectangle = new Rectangle(PLACE_X, PLACE_Y, WIDTH, HEIGHT);
    }

    @Override
    public Role1 settingRoleImage(String imageName) {
        if (Objects.nonNull(imageName)) imageFileName = imageName;
        if ("null".equals(imageName)) texture = null;
        else if (imageName != null)
            texture = new Texture(new FileHandle(new File(PathUtils.generateImagePath(imageName))));
        return this;
    }

    @Override
    public boolean checkIsShowOnScreen() {
        return !(texture == null);
    }

    @Override
    public float[] takePlacePoint() {
        return new float[]{PLACE_X, PLACE_Y};
    }

    @Override
    public float[] takeSize() {
        return new float[]{WIDTH, HEIGHT};
    }

    @Override
    public Rectangle getRoleRectangle() {
        return rectangle;
    }

    @Override
    public Texture getTexture() {
        return texture;
    }

    @Override
    public void draw(SpriteBatch batch) {
        batch.draw(getTexture(), PLACE_X, PLACE_Y, WIDTH, HEIGHT);
    }

    @Override
    public void dispose() {
        if (Objects.nonNull(texture)) texture.dispose();
    }

    @Override
    public String getImageName() {
        if (Objects.isNull(texture)) return "null";
        else return imageFileName;
    }
}
