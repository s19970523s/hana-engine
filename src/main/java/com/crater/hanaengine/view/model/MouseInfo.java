package com.crater.hanaengine.view.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

public class MouseInfo {
    private final Vector3 mouseVector;
    private final float[] cameraPlace;
    private final Rectangle mouseRectangle;

    public MouseInfo() {
        mouseVector = new Vector3();
        cameraPlace = new float[2];
        mouseRectangle = new Rectangle(0, 0, 100, 100);
    }

    public float[] getCameraPlace(OrthographicCamera camera) {
        mouseVector.set(Gdx.input.getX(), Gdx.input.getY(), 0);
        camera.unproject(mouseVector);
        cameraPlace[0] = mouseVector.x;
        cameraPlace[1] = mouseVector.y;
        return cameraPlace;
    }

    public Rectangle getMouseRectangle(OrthographicCamera camera) {
        mouseVector.set(Gdx.input.getX(), Gdx.input.getY(), 0);
        camera.unproject(mouseVector);
        mouseRectangle.x = mouseVector.x;
        mouseRectangle.y = mouseVector.y;
        return mouseRectangle;
    }

    public boolean isLeftClick() {
        return Gdx.input.isButtonPressed(Input.Buttons.LEFT);
    }

    public boolean isLeftJustPressedClick() {
        return Gdx.input.isButtonJustPressed(Input.Buttons.LEFT);
    }

    public boolean isRightClick() {
        return Gdx.input.isButtonPressed(Input.Buttons.RIGHT);
    }
}
