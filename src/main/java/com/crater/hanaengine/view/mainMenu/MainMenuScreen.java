package com.crater.hanaengine.view.mainMenu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.crater.hanaengine.exception.FrontEndException;
import com.crater.hanaengine.view.HanaGame;
import com.crater.hanaengine.view.chat.ChatScreen;
import com.crater.hanaengine.view.utils.FrontEndCommunication;
import com.crater.hanaengine.view.utils.PathUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

public class MainMenuScreen implements Screen {
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private final static String USER_ID = "test";
    private final HanaGame hanaGame;
    private FitViewport viewport;
    private Stage stage;
    private MainMenuSkin mainMenuSkin;
    private MainMenuCg mainMenuCg;
    private NewGameItem newGameItem;
    private ContinueItem continueItem;
    private ConfigItem configItem;
    private PopupYesButton popupYesButton;
    private Music bgm;
    Image configNotFinishMessage;


    public MainMenuScreen(HanaGame hanaGame) {
        this.hanaGame = hanaGame;
    }


    @Override
    public void show() {
        try {
            initViewport();
            stage = new Stage(viewport, hanaGame.batch);
            mainMenuSkin = new MainMenuSkin();
            initMainMenuCg();
            initNewGameItem();
            initContinueItem();
            initConfigItem();
            initConfigMessage();
            initPopUpYesButton();
            settingBgm();
            Gdx.input.setInputProcessor(stage);
        } catch (Exception e) {
            log.error("main menu screen show fail");
            log.error(ExceptionUtils.getStackTrace(e));
            closeEngine();
            throw e;
        }
    }

    private void initViewport() {
        try {
            OrthographicCamera camera = new OrthographicCamera();
            camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            viewport = new FitViewport(1920, 1080, camera);
        } catch (Exception e) {
            throw new FrontEndException("init view port fail", e);
        }
    }

    private void initMainMenuCg() {
        try {
            mainMenuCg = new MainMenuCg(mainMenuSkin);
            stage.addActor(mainMenuCg);
        } catch (Exception e) {
            throw new FrontEndException("init main menu background fail", e);
        }
    }

    private void initNewGameItem() {
        try {
            newGameItem = new NewGameItem(mainMenuSkin);
            stage.addActor(newGameItem);
        } catch (Exception e) {
            throw new FrontEndException("init new game item fail", e);
        }
    }

    private void initContinueItem() {
        try {
            continueItem = new ContinueItem(mainMenuSkin);
            stage.addActor(continueItem);
            var isHaveSaveData = FrontEndCommunication.checkHaveSaveData(USER_ID);
            continueItem.setVisible(isHaveSaveData);
        } catch (Exception e) {
            throw new FrontEndException("init continue item fail", e);
        }
    }

    private void initConfigItem() {
        try {
            configItem = new ConfigItem(mainMenuSkin);
            stage.addActor(configItem);
        } catch (Exception e) {
            throw new FrontEndException("init config item fail", e);
        }
    }

    private void initConfigMessage() {
        try {
            configNotFinishMessage = new Image(new Texture(new FileHandle(new File(PathUtils.generateImagePath("configMessage.png")))));
            configNotFinishMessage.setSize(1280, 720);
            configNotFinishMessage.setPosition(350, 200);
            stage.addActor(configNotFinishMessage);
            configNotFinishMessage.setVisible(false);
        } catch (Exception e) {
            throw new FrontEndException("init config Message fail", e);
        }
    }

    private void initPopUpYesButton() {
        try {
            popupYesButton = new PopupYesButton(mainMenuSkin);
            stage.addActor(popupYesButton);
            popupYesButton.setVisible(false);
        } catch (Exception e) {
            throw new FrontEndException("init popup yes button fail", e);
        }
    }

    private void settingBgm() {
        try {
            String bgmFileName = "mainMenuBgm.mp3";
            bgm = Gdx.audio.newMusic(new FileHandle(new File(PathUtils.generateMusicPath(bgmFileName))));
            bgm.setLooping(true);
            bgm.setVolume(0.5F);
            bgm.play();
        } catch (Exception e) {
            throw new FrontEndException("setting bgm fail", e);
        }
    }

    @Override
    public void render(float delta) {
        try {
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
            stage.act(delta);
            stage.draw();
            if (newGameItem.isClicked()) doNewGame();
            if (continueItem.isClicked()) doLoadGame();
            if (configItem.isClicked()) {
                showConfigMessage();
            }
            if (popupYesButton.isClicked()) closeConfigMessage();
            newGameItem.resetClickStatus();
            continueItem.resetClickStatus();
            configItem.resetClickStatus();
            popupYesButton.resetClickStatus();
        } catch (Exception e) {
            log.error(ExceptionUtils.getStackTrace(e));
            closeEngine();
            throw e;
        }
    }

    private void doNewGame() {
        try {
            hanaGame.setScreen(new ChatScreen(hanaGame));
            dispose();
        } catch (Exception e) {
            throw new FrontEndException("do new game fail", e);
        }
    }

    private void doLoadGame() {
        try {
            hanaGame.setScreen(new ChatScreen(hanaGame, true));
            dispose();
        } catch (Exception e) {
            throw new FrontEndException("do load game fail", e);
        }
    }

    private void showConfigMessage() {
        configNotFinishMessage.setVisible(true);
        popupYesButton.setVisible(true);
    }

    private void closeConfigMessage() {
        configNotFinishMessage.setVisible(false);
        popupYesButton.setVisible(false);
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        bgm.stop();
        stage.dispose();
    }

    private void closeEngine() {
        try {
            bgm.stop();
            stage.dispose();
        } finally {
            hanaGame.dispose();
        }
    }
}
