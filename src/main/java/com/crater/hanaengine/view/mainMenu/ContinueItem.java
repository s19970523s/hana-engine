package com.crater.hanaengine.view.mainMenu;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.crater.hanaengine.view.mainMenu.enums.MainMenuItemData;

public class ContinueItem extends ImageButton {
    private boolean isClicked = false;

    public ContinueItem(MainMenuSkin skin) {
        super(skin, MainMenuItemData.continueItemForMenu.getData().key());
        var continueItemData = MainMenuItemData.continueItemForMenu.getData();
        this.setSize(continueItemData.width(), continueItemData.height());
        this.setPosition(continueItemData.placeX(), continueItemData.placeY());
        this.addListener(new ClickListener() { // 覆寫 clicked 方法
            @Override
            public void clicked(InputEvent event, float x, float y) {
                isClicked = true;
            }
        });
    }

    public void resetClickStatus() {
        isClicked = false;
    }

    public boolean isClicked() {
        return isClicked;
    }
}