package com.crater.hanaengine.view.mainMenu;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.crater.hanaengine.view.mainMenu.enums.MainMenuItemData;

public class NewGameItem extends ImageButton {
    private boolean isClicked = false;
    //libgdx 的解析度不同導致setPosition 的 x y 位置不同，該怎麼解決
    public NewGameItem(MainMenuSkin skin) {
        super(skin, MainMenuItemData.newGameForMenu.getData().key());
        var newGameForMenuItemData = MainMenuItemData.newGameForMenu.getData();
        this.setSize(newGameForMenuItemData.width(), newGameForMenuItemData.height());
        this.setPosition(newGameForMenuItemData.placeX(), newGameForMenuItemData.placeY());
        this.addListener(new ClickListener() { // 覆寫 clicked 方法
            @Override
            public void clicked(InputEvent event, float x, float y) {
                isClicked = true;
            }
        });
    }

    public void resetClickStatus() {
        isClicked = false;
    }

    public boolean isClicked() {
        return isClicked;
    }
}
