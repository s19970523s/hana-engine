package com.crater.hanaengine.view.mainMenu;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.crater.hanaengine.view.mainMenu.enums.MainMenuItemData;
import com.crater.hanaengine.view.utils.PathUtils;

public class MainMenuSkin extends Skin {

    public MainMenuSkin() {
        super();
        addMainMenuBackgroundImage();
        addNewGameItemStyle();
        addContinueItemStyle();
        addConfigForMenuItemStyle();
        addPopUpYesButtonStyle();
    }

    private void addMainMenuBackgroundImage() {
        this.add(MainMenuItemData.mainMenuCg.getData().key(),
                new Texture(PathUtils.generateImagePath(MainMenuItemData.mainMenuCg.getData().fileName())));
    }

    private void addNewGameItemStyle() {
        var newGameStyle = new ImageButton.ImageButtonStyle();
        newGameStyle.imageUp = new TextureRegionDrawable(new TextureRegion(
                new Texture(PathUtils.generateImagePath(MainMenuItemData.newGameForMenu.getData().fileName()))));
        this.add(MainMenuItemData.newGameForMenu.getData().key(), newGameStyle);
    }

    private void addContinueItemStyle() {
        var continueItemStyle = new ImageButton.ImageButtonStyle();
        continueItemStyle.imageUp = new TextureRegionDrawable(new TextureRegion(
                new Texture(PathUtils.generateImagePath(MainMenuItemData.continueItemForMenu.getData().fileName()))));
        this.add(MainMenuItemData.continueItemForMenu.getData().key(), continueItemStyle);
    }

    private void addConfigForMenuItemStyle() {
        var configItemStyle = new ImageButton.ImageButtonStyle();
        configItemStyle.imageUp = new TextureRegionDrawable(new TextureRegion(
                new Texture(PathUtils.generateImagePath(MainMenuItemData.configForMenu.getData().fileName()))));
        this.add(MainMenuItemData.configForMenu.getData().key(), configItemStyle);
    }

        private void addPopUpYesButtonStyle() {
        var popUpYesButton = new ImageButton.ImageButtonStyle();
        popUpYesButton.imageUp = new TextureRegionDrawable(new TextureRegion(
                new Texture(PathUtils.generateImagePath(MainMenuItemData.popUpYesButton.getData().fileName()))));
        this.add(MainMenuItemData.popUpYesButton.getData().key(), popUpYesButton);
    }
}
