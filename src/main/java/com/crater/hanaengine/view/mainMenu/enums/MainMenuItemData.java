package com.crater.hanaengine.view.mainMenu.enums;

public enum MainMenuItemData {
    mainMenuCg(new Data("mainMenuCg", "mainMenuCg.png", 1920, 1080, 0, 0)),
    newGameForMenu(new Data("newGameForMenu", "newGameForMenu.png", 400, 200, 50, 800)),
    continueItemForMenu(new Data("continueItemForMenu", "continueItemForMenu.png", 400, 200, 50, 500)),
    configForMenu(new Data("configForMenu", "configForMenu.png", 400, 200, 50, 200)),
    popUpYesButton(new Data("popUpYesButton", "popUpYesButton.png", 400, 200, 900, 300));
    private final Data data;

    MainMenuItemData(Data data) {
        this.data = data;
    }

    public Data getData() {
        return data;
    }

    public record Data(String key, String fileName, float width, float height, float placeX, float placeY) {

    }
}
