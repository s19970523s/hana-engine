package com.crater.hanaengine.view.mainMenu;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.crater.hanaengine.view.model.MouseInfo;

public interface MenuItem {
    MenuItem settingImage(String imageName);
    boolean checkIsShowOnScreen();

    /**
     * take role x and y
     * @return float[x, y]
     */
    float[] takePlacePoint();

    /**
     * take role image and rectangle size
     * @return float[width, height]
     */
    float[] takeSize();

    Rectangle getRoleRectangle();

    Texture getTexture();

    void settingIsShowOnScreen(boolean isShowOnScreen);

    void draw(SpriteBatch batch);

    void disposeTexture();

    boolean isTouch(MouseInfo mouseInfo, OrthographicCamera camera);

    boolean isClick(MouseInfo mouseInfo, OrthographicCamera camera);
}
