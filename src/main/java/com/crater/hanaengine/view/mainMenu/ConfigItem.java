package com.crater.hanaengine.view.mainMenu;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.crater.hanaengine.view.mainMenu.enums.MainMenuItemData;

public class ConfigItem extends ImageButton {
    private boolean isClicked = false;

    public ConfigItem(MainMenuSkin skin) {
        super(skin, MainMenuItemData.configForMenu.getData().key());
        var configForMenuItemData = MainMenuItemData.configForMenu.getData();
        setSize(configForMenuItemData.width(), configForMenuItemData.height());
        setPosition(configForMenuItemData.placeX(), configForMenuItemData.placeY());
        addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                isClicked = true;
            }
        });
    }

    public void resetClickStatus() {
        isClicked = false;
    }

    public boolean isClicked() {
        return isClicked;
    }
}
