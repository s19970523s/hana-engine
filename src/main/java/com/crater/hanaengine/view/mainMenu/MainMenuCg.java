package com.crater.hanaengine.view.mainMenu;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.crater.hanaengine.view.mainMenu.enums.MainMenuItemData;

public class MainMenuCg extends Image {

    public MainMenuCg(MainMenuSkin mainMenuSkin) {
        super(mainMenuSkin, MainMenuItemData.mainMenuCg.getData().key());
        var mainMenuCgData = MainMenuItemData.mainMenuCg.getData();
        super.setPosition(mainMenuCgData.placeX(), mainMenuCgData.placeY());
        super.setSize(mainMenuCgData.width(), mainMenuCgData.height());
    }
}
