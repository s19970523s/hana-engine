package com.crater.hanaengine.view.utils;

import java.nio.file.Paths;

public class PathUtils {
    public static String generateImagePath(String imageName) {
        var path = Paths.get("");
        return path.toAbsolutePath().normalize() + "/material/" + "/img/" + imageName;
    }

    public static String generateFontPath(String fontName) {
        var path = Paths.get("");
        return path.toAbsolutePath().normalize() + "/material/" + "/font/" + fontName;
    }

    public static String generateMusicPath(String musicName) {
        var path = Paths.get("");
        return path.toAbsolutePath().normalize() + "/material/" + "/music/" + musicName;
    }
}
