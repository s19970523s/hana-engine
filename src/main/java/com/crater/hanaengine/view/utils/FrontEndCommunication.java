package com.crater.hanaengine.view.utils;

import com.crater.hanaengine.bean.response.chat.ChatResponse;
import com.crater.hanaengine.bean.response.saveData.GetSaveDataResponse;
import com.crater.hanaengine.bean.response.saveData.NewSaveDataResponse;
import com.crater.hanaengine.bean.response.saveData.SaveDataRequest;
import com.crater.hanaengine.utils.HttpClientUtil;
import com.crater.hanaengine.view.Dto.ChatDto;
import com.crater.hanaengine.view.Dto.SaveDataDto;
import com.crater.hanaengine.view.mainMenu.RoleMapKey;

import java.util.HashMap;
import java.util.Map;

public class FrontEndCommunication {
    private enum apiPath {
        chat("chat");
        private final String path;

        apiPath(String path) {
            this.path = path;
        }

        public String getPath() {
            return path;
        }
    }

    private static final String URL = "http://127.0.0.1:8080/hanaEngine";

    public static ChatDto callGetChat(String nextPath, String userId) {
        var chatUtl = URL + "/" + apiPath.chat.getPath() + nextPath;
        var header = generateRequestHeader(userId);
        var chatResponse = HttpClientUtil.sendGetHttp(chatUtl, header, ChatResponse.class);
        var role1 = chatResponse.getRole1();
        var role2 = chatResponse.getRole2();
        var role3 = chatResponse.getRole3();
        var result = new ChatDto().setContent(chatResponse.getChatContent())
                .setBackgroundImageName(chatResponse.getBackgroundImage()).setNextPath(chatResponse.getNextPath());
        if (role1 != null) result.setRole1ImageName(role1.getFileName());
        if (role2 != null) result.setRole2ImageName(role2.getFileName());
        if (role3 != null) result.setRole3ImageName(role3.getFileName());
        result.setBgm(chatResponse.getBgm()).setSoundEffect(chatResponse.getSoundEffect());
        return result;
    }

    public static void saveData(SaveDataDto saveDataDto) {
        try {
            var saveDataRequest = new SaveDataRequest().setSaveId(1L).setCurrentUrlPath(saveDataDto.currentApiPath())
                    .setRemark(saveDataDto.remark()).setBackgroundFileName(saveDataDto.backgroundImage().getImageFileName())
                    .setBgmFileName(String.valueOf(saveDataDto.bgmFileName()))
                    .setRole1FileName(saveDataDto.roleMap().get(RoleMapKey.role1).getImageName())
                    .setRole2FileName(saveDataDto.roleMap().get(RoleMapKey.role2).getImageName())
                    .setRole3FileName(saveDataDto.roleMap().get(RoleMapKey.role3).getImageName())
                    .setCurrentText(saveDataDto.contentText());
            var saveDataResponse = HttpClientUtil.sendPostHttpByJson(
                    "http://localhost:8080/hanaEngine/saveData/update", null, saveDataRequest,
                    NewSaveDataResponse.class);
            if (!"0".equals(saveDataResponse.getErrorCode())) throw new RuntimeException("have error for save");
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException("call saveData backend fail.", e);
        }
    }

    public static boolean checkHaveSaveData(String userId) {
        var saveDataUrl = URL + "/saveData/getList";
        var header = generateRequestHeader(userId);
        var response = HttpClientUtil.sendGetHttp(saveDataUrl, header, GetSaveDataResponse.class);
        return response.getSaveData().size() == 1 && !"DEFUAL_SAVE".equals(response.getSaveData().get(0).remark());
    }

    public static ChatDto getSaveData(String userId) {
        var saveDataUrl = URL + "/saveData/getList";
        var header = generateRequestHeader(userId);
        var response = HttpClientUtil.sendGetHttp(saveDataUrl, header, GetSaveDataResponse.class);
        return generateChatDto(response);
    }

    private static ChatDto generateChatDto(GetSaveDataResponse response) {
        var saveData = response.getSaveData().get(0);
        var lineNumber = Integer.valueOf(saveData.currentPath().split("/")[2]);
        return new ChatDto().setNextPath(saveData.nextPath()).setFileLineNumber(lineNumber)
                .setRole1ImageName(saveData.role1FileName()).setRole2ImageName(saveData.role2FileName())
                .setRole3ImageName(saveData.role3FileName()).setContent(saveData.currentText())
                .setBackgroundImageName(saveData.backgroundFileName()).setBgm(saveData.bgmFileName());
    }

    private static Map<String, String> generateRequestHeader(String userId) {
        Map<String, String> result = new HashMap<>();
        result.put("saveId", userId);
        return result;
    }
}
