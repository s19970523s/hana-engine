package com.crater.hanaengine.view.save;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public interface SaveDataBlock {
    void settingPlace(float x, float y);

    void settingSize(float width, float high);
    SaveDataBlock settingImage(String imageName);
    boolean checkIsShowOnScreen();

    /**
     * take role x and y
     * @return float[x, y]
     */
    float[] takePlacePoint();

    /**
     * take role image and rectangle size
     * @return float[width, height]
     */
    float[] takeSize();

    Rectangle getRoleRectangle();

    Texture getTexture();

    void settingIsShowOnScreen(boolean isShowOnScreen);

    void draw(SpriteBatch batch);

    void disposeTexture();
}
