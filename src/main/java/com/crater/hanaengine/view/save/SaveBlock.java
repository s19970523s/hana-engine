package com.crater.hanaengine.view.save;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public class SaveBlock implements SaveDataBlock{
    private float placeX;
    private float placeY;
    private float width;
    private float high;
    private static final String background = "saveBlock.png";

    @Override
    public void settingPlace(float x, float y) {
        placeX = x;
        placeY = y;
    }

    @Override
    public void settingSize(float width, float high) {
        this.width = width;
        this.high = high;
    }

    @Override
    public SaveDataBlock settingImage(String imageName) {
        return null;
    }

    @Override
    public boolean checkIsShowOnScreen() {
        return false;
    }

    @Override
    public float[] takePlacePoint() {
        return new float[0];
    }

    @Override
    public float[] takeSize() {
        return new float[0];
    }

    @Override
    public Rectangle getRoleRectangle() {
        return null;
    }

    @Override
    public Texture getTexture() {
        return null;
    }

    @Override
    public void settingIsShowOnScreen(boolean isShowOnScreen) {

    }

    @Override
    public void draw(SpriteBatch batch) {

    }

    @Override
    public void disposeTexture() {

    }
}
