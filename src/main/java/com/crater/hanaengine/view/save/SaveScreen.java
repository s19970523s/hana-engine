package com.crater.hanaengine.view.save;

import com.badlogic.gdx.Screen;
import com.crater.hanaengine.view.HanaGame;

public class SaveScreen implements Screen {

    private HanaGame hanaGame;

    public SaveScreen(HanaGame hanaGame) {
        this.hanaGame = hanaGame;
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
