package com.crater.hanaengine.enums;

public enum Function {
    fileName("fileName", new Level[]{Level.background, Level.backgroundMusic, Level.role1,
            Level.role2, Level.role3, Level.sound}),
    cutscene("cutscene", new Level[] {Level.system}),
    jumpAttachedComponentsItem("jumpItem", new Level[]{Level.role1, Level.role2, Level.role3}),
    action("action", new Level[] {Level.role1, Level.role2, Level.role3});
    private String functionString;
    private Level[] correspondingLevel;

    Function(String taxonomyString, Level[] correspondingLevel) {
        this.functionString = taxonomyString;
        this.correspondingLevel = correspondingLevel;
    }

    public String getFunctionString() {
        return functionString;
    }

    public Level[] getCorrespondingLevel() {
        return correspondingLevel;
    }
}
