package com.crater.hanaengine.enums;

public enum Level {
    background("background"), backgroundMusic("bgm"), role1("role1"), role2("role2"),
    role3("role3"), sound("sound"), system("system");
    private String levelContent;

    Level(String levelContent) {
        this.levelContent = levelContent;
    }

    public String getLevelContent() {
        return levelContent;
    }
}
