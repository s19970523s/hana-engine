package com.crater.hanaengine.enums;

public enum ApiHeaderParameter {
    userId("userId");

    private String headerContent;

    ApiHeaderParameter(String headerContent) {
        this.headerContent = headerContent;
    }

    public String getHeaderContent() {
        return headerContent;
    }

}
