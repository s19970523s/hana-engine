CREATE TABLE IF NOT EXISTS HA_TB_SAVE
(
    ID                      INTEGER   not null
        primary key,
    REMARKS                 CHARACTER VARYING(50),
    SCRIPT_FILE_NAME        CHARACTER VARYING(50),
    SCRIPT_FILE_LINE_NUMBER INTEGER,
    BACKGROUND_FILE_NAME    CHARACTER VARYING(50),
    ROLE1_FILE_NAME         CHARACTER VARYING(50),
    ROLE2_FILE_NAME         CHARACTER VARYING(50),
    ROLE3_FILE_NAME         CHARACTER VARYING(50),
    BGM_FILE_NAME           CHARACTER VARYING(50),
    CURRENT_TEXT            CHARACTER VARYING(500),
    CREATE_DATE             TIMESTAMP not null,
    UPDATE_DATE             TIMESTAMP not null
);

CREATE TABLE IF NOT EXISTS HA_TB_FLAG
(
    ID          INT          NOT NULL AUTO_INCREMENT,
    SAVE_ID     INT          NOT NULL,
    FLAG_NAME   VARCHAR2(50) NOT NULL,
    IS_STAND    NUMBER(1) DEFAULT 0,
    CREATE_DATE DATETIME     NOT NULL,
    PRIMARY KEY (ID)
);

INSERT INTO HA_TB_SAVE (ID, REMARKS, SCRIPT_FILE_NAME, SCRIPT_FILE_LINE_NUMBER, BACKGROUND_FILE_NAME, ROLE1_FILE_NAME,
                        ROLE2_FILE_NAME, ROLE3_FILE_NAME, CREATE_DATE, UPDATE_DATE)
SELECT 1,
       'DEFUAL_SAVE',
       'main.hana',
       0,
       'null',
       'null',
       'null',
       'null',
       CURRENT_TIMESTAMP(),
       CURRENT_TIMESTAMP()
FROM DUAL
where not exists(select 1 from HA_TB_SAVE where ID = 1)