package com.crater.hanaengine.service.impl;

import com.crater.hanaengine.bean.configurationProperties.ScriptProperties;
import com.crater.hanaengine.bean.dto.ScriptDto;
import com.crater.hanaengine.exception.HanaScriptException;
import com.crater.hanaengine.utils.ReadScriptFileUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ScriptServiceImplTest {
    ScriptServiceImpl readScriptService = new ScriptServiceImpl();
    ScriptProperties scriptProperties;

    @BeforeEach
    void setUp() {
        scriptProperties = Mockito.mock(ScriptProperties.class);
    }

    @Test
    public void readScriptContentAndInstructionTest_targetLineNumber0_haveInstructionSuccess() {
        try (MockedStatic<ReadScriptFileUtil> readScriptFileUtil =
                     Mockito.mockStatic(ReadScriptFileUtil.class)) {
            readScriptFileUtil.when(() -> ReadScriptFileUtil.readScript(Mockito.anyString()))
                    .thenAnswer(i -> {
                        String inputScriptName = i.getArgument(0);
                        Assertions.assertEquals("testScript1.hana", inputScriptName);
                        return Optional.of(generateScriptContent());
                    });
            ScriptDto scriptDto = readScriptService.readScriptContentAndInstruction("testScript1.hana",
                    0L);
            List<String> scriptInstruction = scriptDto.getScriptInstruction()
                    .orElseThrow(() -> new RuntimeException("getScriptInstruction can't be null"));
            String role2FileName = scriptInstruction.get(0);
            String role2Cutscene = scriptInstruction.get(1);
            Assertions.assertEquals("hana.role2.fileName(\"test.png\")", role2FileName);
            Assertions.assertEquals("hana.role2.cutscene(\"little jump\")", role2Cutscene);
            Assertions.assertEquals("你還好嗎?", scriptDto.getScriptContent()
                    .orElseThrow(() -> new RuntimeException("getScriptContent is can't be null")));
            Assertions.assertEquals(2, scriptDto.getScriptFileEndLineNumber());
            Assertions.assertFalse(scriptDto.isBottomOfFile());
        }
    }

    @Test
    public void readScriptContentAndInstructionTest_targetLineNumber3_InstructionHave1Success() {
        try (MockedStatic<ReadScriptFileUtil> readScriptFileUtil =
                     Mockito.mockStatic(ReadScriptFileUtil.class)) {
            readScriptFileUtil.when(() -> ReadScriptFileUtil.readScript(Mockito.anyString()))
                    .thenAnswer(i -> {
                        String inputScriptName = i.getArgument(0);
                        Assertions.assertEquals("testScript1.hana", inputScriptName);
                        return Optional.of(generateScriptContent());
                    });
            ScriptDto scriptDto = readScriptService.readScriptContentAndInstruction("testScript1.hana",
                    3L);
            List<String> scriptInstruction = scriptDto.getScriptInstruction()
                    .orElseThrow(() -> new RuntimeException("getScriptInstruction can't be null"));
            String role2FileName = scriptInstruction.get(0);
            Assertions.assertEquals("hana.role2.fileName(\"test2.png\")", role2FileName);
            Assertions.assertEquals("讓我看一下", scriptDto.getScriptContent()
                    .orElseThrow(() -> new RuntimeException("getScriptContent is can't be null")));
            Assertions.assertEquals(4, scriptDto.getScriptFileEndLineNumber());
            Assertions.assertFalse(scriptDto.isBottomOfFile());
        }
    }

    @Test
    public void readScriptContentAndInstructionTest_targetLineNumber4_InstructionIsNullSuccess() {
        try (MockedStatic<ReadScriptFileUtil> readScriptFileUtil =
                     Mockito.mockStatic(ReadScriptFileUtil.class)) {
            readScriptFileUtil.when(() -> ReadScriptFileUtil.readScript(Mockito.anyString()))
                    .thenAnswer(i -> {
                        String inputScriptName = i.getArgument(0);
                        Assertions.assertEquals("testScript1.hana", inputScriptName);
                        return Optional.of(generateScriptContent());
                    });
            ScriptDto scriptDto = readScriptService.readScriptContentAndInstruction("testScript1.hana",
                    4L);
            Assertions.assertNull(scriptDto.getScriptInstruction().orElse(null));
            Assertions.assertEquals("讓我看一下", scriptDto.getScriptContent()
                    .orElseThrow(() -> new RuntimeException("getScriptContent is can't be null")));
            Assertions.assertEquals(4, scriptDto.getScriptFileEndLineNumber());
            Assertions.assertFalse(scriptDto.isBottomOfFile());
        }
    }

    @Test
    public void readScriptContentAndInstructionTest_targetLineNumber5_InstructionIsNullIsBottomOfFileIsTrue() {
        try (MockedStatic<ReadScriptFileUtil> readScriptFileUtil =
                     Mockito.mockStatic(ReadScriptFileUtil.class)) {
            readScriptFileUtil.when(() -> ReadScriptFileUtil.readScript(Mockito.anyString()))
                    .thenAnswer(i -> {
                        String inputScriptName = i.getArgument(0);
                        Assertions.assertEquals("testScript1.hana", inputScriptName);
                        return Optional.of(generateScriptContent());
                    });
            ScriptDto scriptDto = readScriptService.readScriptContentAndInstruction("testScript1.hana",
                    5L);
            Assertions.assertNull(scriptDto.getScriptInstruction().orElse(null));
            Assertions.assertEquals("看起來受傷的並不嚴重", scriptDto.getScriptContent()
                    .orElseThrow(() -> new RuntimeException("getScriptContent is can't be null")));
            Assertions.assertEquals(5, scriptDto.getScriptFileEndLineNumber());
            Assertions.assertTrue(scriptDto.isBottomOfFile());
        }
    }

    @Test
    public void readScriptContentAndInstructionTest_targetLineNumber100_throwHanaScriptException() {
        try (MockedStatic<ReadScriptFileUtil> readScriptFileUtil =
                     Mockito.mockStatic(ReadScriptFileUtil.class)) {
            readScriptFileUtil.when(() -> ReadScriptFileUtil.readScript(Mockito.anyString()))
                    .thenAnswer(i -> {
                        String inputScriptName = i.getArgument(0);
                        Assertions.assertEquals("testScript1.hana", inputScriptName);
                        return Optional.of(generateScriptContent());
                    });
            HanaScriptException exception = Assertions.assertThrows(HanaScriptException.class,
                    () -> readScriptService.readScriptContentAndInstruction("testScript1.hana",
                    100L));
            Assertions.assertEquals("targetLineNumber is over to allScriptContent size",
                    exception.getMessage());
        }
    }

    @Test
    public void readScriptContentAndInstructionTest_ReadFileUtilReturnEmpty_throwHanaScriptException() {
        try (MockedStatic<ReadScriptFileUtil> readScriptFileUtil =
                     Mockito.mockStatic(ReadScriptFileUtil.class)) {
            readScriptFileUtil.when(() -> ReadScriptFileUtil.readScript(Mockito.anyString()))
                    .thenReturn(Optional.empty());
            HanaScriptException exception = Assertions.assertThrows(HanaScriptException.class,
                    () -> readScriptService.readScriptContentAndInstruction("testScript1.hana",
                            0L));
            Assertions.assertEquals("ReadScriptFileUtil readScript fail", exception.getMessage());
        }
    }

    private List<String> generateScriptContent() {
        List<String> result = new ArrayList<>();
        result.add("hana.role2.fileName(\"test.png\")");
        result.add("hana.role2.cutscene(\"little jump\")");
        result.add("你還好嗎?");
        result.add("hana.role2.fileName(\"test2.png\")");
        result.add("讓我看一下");
        result.add("看起來受傷的並不嚴重");
        return result;
    }
}
