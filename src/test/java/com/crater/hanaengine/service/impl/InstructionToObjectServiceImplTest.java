package com.crater.hanaengine.service.impl;

import com.crater.hanaengine.bean.dto.InstructionDto;
import com.crater.hanaengine.enums.Function;
import com.crater.hanaengine.enums.Level;
import com.crater.hanaengine.exception.HanaInstructionException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class InstructionToObjectServiceImplTest {

    //test Instruction
    String test1 = "hana.bgm.fileName(\"fileName\")";
    String test2 = "hana.role1.fileName(\"fileName\")";
    String test3 = "hana.role1.action(\"xxx\")";
    String test5 = "hana.role1.jumpItem(\"xxx\")";
    String test6 = "hana.role2.fileName(\"fileName\")";
    String test7 = "hana.role2.action(\"xxx\")";
    String test9 = "hana.role2.jumpItem(\"fileName\")";
    String test10 = "hana.role3.fileName(\"fileName\")";
    String test11 = "hana.role3.action(\"xxx\")";
    String test13 = "hana.role3.jumpItem(\"fileName\")";
    String test14 = "hana.system.cutscene(\"xxxx\")";
    String test15 = "xxx.xxx.xxx()";
    String test16 = "hana.xxx.xxx()";
    String test17 = "hana.role1.xxx()";
    String test18 = "hana.role1.cutscene(\"xxxx\")";

    private InstructionToObjectServiceImpl target = new InstructionToObjectServiceImpl();

    @BeforeEach
    void setUp() {

    }

    @Test
    public void parsingInstruction_test1_success() {
        InstructionDto result = target.parsingInstruction(test1);
        Assertions.assertEquals(Level.backgroundMusic, result.getLevel());
        Assertions.assertEquals(Function.fileName, result.getFunction());
        Assertions.assertEquals("fileName", result.getValue());
    }

    @Test
    public void parsingInstruction_test2_success() {
        InstructionDto result = target.parsingInstruction(test2);
        Assertions.assertEquals(Level.role1, result.getLevel());
        Assertions.assertEquals(Function.fileName, result.getFunction());
        Assertions.assertEquals("fileName", result.getValue());
    }

    @Test
    public void parsingInstruction_test3_success() {
        InstructionDto result = target.parsingInstruction(test3);
        Assertions.assertEquals(Level.role1, result.getLevel());
        Assertions.assertEquals(Function.action, result.getFunction());
        Assertions.assertEquals("xxx", result.getValue());
    }

    @Test
    public void parsingInstruction_test5_success() {
        InstructionDto result = target.parsingInstruction(test5);
        Assertions.assertEquals(Level.role1, result.getLevel());
        Assertions.assertEquals(Function.jumpAttachedComponentsItem, result.getFunction());
        Assertions.assertEquals("xxx", result.getValue());
    }

    @Test
    public void parsingInstruction_test6_success() {
        InstructionDto result = target.parsingInstruction(test6);
        Assertions.assertEquals(Level.role2, result.getLevel());
        Assertions.assertEquals(Function.fileName, result.getFunction());
        Assertions.assertEquals("fileName", result.getValue());
    }

    @Test
    public void parsingInstruction_test7_success() {
        InstructionDto result = target.parsingInstruction(test7);
        Assertions.assertEquals(Level.role2, result.getLevel());
        Assertions.assertEquals(Function.action, result.getFunction());
        Assertions.assertEquals("xxx", result.getValue());
    }

    @Test
    public void parsingInstruction_test9_success() {
        InstructionDto result = target.parsingInstruction(test9);
        Assertions.assertEquals(Level.role2, result.getLevel());
        Assertions.assertEquals(Function.jumpAttachedComponentsItem, result.getFunction());
        Assertions.assertEquals("fileName", result.getValue());
    }

    @Test
    public void parsingInstruction_test10_success() {
        InstructionDto result = target.parsingInstruction(test10);
        Assertions.assertEquals(Level.role3, result.getLevel());
        Assertions.assertEquals(Function.fileName, result.getFunction());
        Assertions.assertEquals("fileName", result.getValue());
    }

    @Test
    public void parsingInstruction_test11_success() {
        InstructionDto result = target.parsingInstruction(test11);
        Assertions.assertEquals(Level.role3, result.getLevel());
        Assertions.assertEquals(Function.action, result.getFunction());
        Assertions.assertEquals("xxx", result.getValue());
    }

    @Test
    public void parsingInstruction_test13_success() {
        InstructionDto result = target.parsingInstruction(test13);
        Assertions.assertEquals(Level.role3, result.getLevel());
        Assertions.assertEquals(Function.jumpAttachedComponentsItem, result.getFunction());
        Assertions.assertEquals("fileName", result.getValue());
    }

    @Test
    public void parsingInstruction_test14_success() {
        InstructionDto result = target.parsingInstruction(test14);
        Assertions.assertEquals(Level.system, result.getLevel());
        Assertions.assertEquals(Function.cutscene, result.getFunction());
        Assertions.assertEquals("xxxx", result.getValue());
    }

    @Test
    public void parsingInstruction_test15_throwHanaInstructionException() {
        HanaInstructionException e = Assertions.assertThrows(HanaInstructionException.class,
                () -> target.parsingInstruction(test15));
        Assertions.assertEquals("this is not Instruction", e.getMessage());
    }

    @Test
    public void parsingInstruction_test16_throwHanaInstructionException() {
        HanaInstructionException e = Assertions.assertThrows(HanaInstructionException.class,
                () -> target.parsingInstruction(test16));
        Assertions.assertEquals("Level not found", e.getMessage());
    }

    @Test
    public void parsingInstruction_test17_throwHanaInstructionException() {
        HanaInstructionException e = Assertions.assertThrows(HanaInstructionException.class,
                () -> target.parsingInstruction(test17));
        Assertions.assertEquals("function name not found", e.getMessage());
    }

    @Test
    public void parsingInstruction_test18_throwHanaInstructionException() {
        HanaInstructionException e = Assertions.assertThrows(HanaInstructionException.class,
                () -> target.parsingInstruction(test18));
        Assertions.assertEquals("function name not found", e.getMessage());
    }
}
