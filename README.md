# 基礎簡介
* hana引擎：主要目標是做出比吉里吉里更簡單更容易跨平台的galgame（雖然目前目標是windows + Linux + Mac）
* hana腳本語言：總共只有五種不同類型的語法，其他地方就跟寫輕小說一樣。，為了讓開發者簡單使用，hana引擎會自動做完一些工作，且目前無法更改。但至少確定可以簡單使用。
# 使用方法
## step1: 安裝java
網路上很多教學，因此這邊不再做教學
## step2: 建立資料夾
先下載hana引擎的jar檔下來。jar檔: https://drive.google.com/file/d/18QwYUp5GxKaWYbV6vsfJkh-4oPT5-Abj/view?usp=sharing

在jar檔的旁邊新增一個資料夾叫做 material。

之後在 material 資料夾裡面新增下列資料夾，分別是
* font
* img
* music
* script

## step3: 安裝字形
再來下載字形檔: https://drive.google.com/file/d/1VYmpRHqQPLz2vtrNm1Sa7e-js9kFONv5/view?usp=sharing

下載完成後解壓縮放到font資料夾，
這部分將來會想辦法放到jar檔裡

## step4: 其他配置
到這步驟就代表你已經完成最基本的配置了，可以開始用hana語言寫腳本了。(hana語言的教學請見該章節)<p>
寫完之後檔名請先取叫 main.hana (目前只允許此檔名)並存到剛剛的script資料夾<p>
接下來
* 請將音樂放到music資料夾裡面
* 請將圖片放到img資料夾裡面

## step5: 開啟遊戲
全部都完成後，開啟命令提示字元使用 cd 移動到該資料夾:
如```cd E:\hana-engin```

接下來繼續使用命令提示字元:```java -jar [下載的jar檔案]```便可開始遊戲

## hana語言教學
目前只支援
* hana.bgm.fileName()
* hana.role1.fileName()
* hana.role2.fileName()
* hana.role3.fileName()
* hana.background.fileName()

由於目前不多所以直接簡單的介紹，當功能越來越豐富之後會再寫wiki來介紹
### hana.bgm.fileName()
設定背景音樂，只需要複製以上內容並且在括弧裡面寫下音樂檔名即可，如:
```hana.bgm.fileName("bgm.ogg")```

### hana.role1.fileName()
接下來的role1，role2，role3都在這邊解釋。<p>
遊戲畫面中最多只會有三個角色的立繪出現。其中role1, 2, 3分別是左中右的位置。<p>
用法也都一樣在括弧中寫下檔名及會更改該位置的圖片，如:
```hana.role2.fileName("女主角1-笑臉.png")```

### hana.background.fileName()
畫面上最多只會有一張背景而已，使用方法一樣在括弧中寫下檔名即可，如:
```hana.background.fileName("background.png")```

# 詳細說明內容
下面的部分比較像開發志之類的東西，如果想要仔細了解並且確認其前瞻性的話再閱讀就好了。

目前版本號：hana腳本語言為0.1版，跟hana引擎為0.2版（之所以是0.2板是因為0.1版胎死腹中了。但畢竟那個想法原本還是存在所以留一個版本號給它紀念一下）

## hana腳本語言：
目前只能存在一個main.hana檔案，所有文本都只能在一個檔案裡。
一個服務於hana引擎的腳本，主要目標讓使用者不用寫到太多像程式碼的東西
其實我個人覺得五種有點多了，但這已經是壓到極限了
這五個分別是：
* background （背景圖片）
* backgroundMusic （背景音樂）
* 立繪
    * role1 （立繪1）：畫面左邊的人
    * role2 （立繪2）：畫面中間的人
    * role3 （立繪3）：畫面右邊的人
* sound （音效）
* system （系統）
  如果系統不算且不須特效的話基本上所有的東西一個叫fileName的東西而已，需要用到腳本的話前面加上"hana."，一般文本的話直接打字上去就好了
  下面直接給個例子：
```
在這個世界上不存在著希望。
為什麼你會這樣想阿？
突然一個聲音從前方傳到我耳邊。
我抬起低著的頭。
hana.background.fileName(頂樓夜景.png)
hana.role1.fileName(女主角1.png)
hana.backgroundMusic.fileName(背景音樂1.wav)
一名少女出現在我眼前。
```
目前還不支援選項選擇跟透過選擇的選項跳劇本以及好感度的功能，但如果是單結局無選項的話可以參考使用。
將來功能，非下個版本目標：
* 提供選項
* 跳腳本
* 根據選項跳腳本
* 新增hana.setting檔案，其用途如下：
    * 指定遊戲標題畫面的圖片
    * 指定標題畫面的哪裡是甚麼按鈕
    * 讓background.fileName等保留字可以字定義，並且在hana引擎透過取代的方式轉譯

## hana引擎0.2版介紹：
第一個可執行版本鎖定在可安裝java的所有平台，並且要求使用者一定要裝java。
現有功能：
* 現在打開就開始了
* 可以正常讀取 hana 0.1 版的指令，並且按enter就會一跳下一句話
* 可以播背景音樂
* 可以換背景
* 可以換人物角色
* 如果有連續的hana指令會在同一個enter執行

下個版本目標：
* 標題畫面
* 跳劇本檔案
* save/load
* 提供選項
## SDK
這是理想，希望可以提供一個網頁工具可以讓使用者可以更簡單寫hana腳本語言。
